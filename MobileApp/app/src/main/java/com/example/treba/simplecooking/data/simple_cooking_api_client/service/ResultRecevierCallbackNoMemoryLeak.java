package com.example.treba.simplecooking.data.simple_cooking_api_client.service;

import com.example.treba.simplecooking.data.simple_cooking_api_client.model.SimpleCookingError;

import java.lang.ref.WeakReference;

/**
 * Created by treba on 29.01.2018.
 */

public class ResultRecevierCallbackNoMemoryLeak<A,T> extends ResultRecevierCallback<T> {
    private WeakReference<A> activityRef;

    public ResultRecevierCallbackNoMemoryLeak(WeakReference<A> activity){
        activityRef = new WeakReference(activity);
    }

    @Override
    public void onSuccess(T ingredient) {
        if(activityRef!=null&&activityRef.get()!=null){
        }
    }

    @Override
    public void onError(SimpleCookingError error) {

        if(activityRef!=null&&activityRef.get()!=null){
        }
    }
}
