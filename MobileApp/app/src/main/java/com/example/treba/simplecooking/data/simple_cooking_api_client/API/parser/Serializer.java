package com.example.treba.simplecooking.data.simple_cooking_api_client.API.parser;

import org.json.JSONObject;

/**
 * Created by treba on 19.01.2018.
 */

public interface Serializer<T> {
     T serialize(JSONObject object);
}
