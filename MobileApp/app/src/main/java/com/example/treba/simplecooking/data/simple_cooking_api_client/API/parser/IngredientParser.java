package com.example.treba.simplecooking.data.simple_cooking_api_client.API.parser;

import android.net.Uri;
import android.os.Debug;
import android.util.Log;

import com.example.treba.simplecooking.data.simple_cooking_api_client.model.Ingredient;
import com.example.treba.simplecooking.data.simple_cooking_api_client.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;

/**
 * Created by User on 20.01.2018.
 */

public class IngredientParser {
    public static  String INGREDIENT="ingredient";
    public static  String INGREDIENTS="ingredients";
    public static String ID="id";
    public static String NAME="name";

    public static Ingredient parse(JSONObject object) {
        if(object==null||!object.has(INGREDIENT)){
            return null;
        }
        try {
            JSONObject ingredient= object.getJSONObject(INGREDIENT);
            return new Ingredient(ingredient.getLong(ID),ingredient.getString(NAME));
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static ArrayList<Ingredient> parseToList(JSONObject object) {
        if(object==null||!object.has(INGREDIENTS)){
            return null;
        }
        ArrayList<Ingredient> ingredients = new ArrayList<Ingredient>();
        try {
            JSONArray array = object.getJSONArray(INGREDIENTS);

            for(int i=0;i<array.length();i++){
                ingredients.add(new Ingredient(array.getJSONObject(i).getLong(ID),array.getJSONObject(i).getString(NAME)));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return ingredients;
    }
}
