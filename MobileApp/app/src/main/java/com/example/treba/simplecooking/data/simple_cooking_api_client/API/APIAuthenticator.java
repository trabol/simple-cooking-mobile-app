package com.example.treba.simplecooking.data.simple_cooking_api_client.API;

import android.util.Log;

import com.example.treba.simplecooking.data.simple_cooking_api_client.API.parser.ErrorParser;
import com.example.treba.simplecooking.data.simple_cooking_api_client.API.parser.LocalUserParser;
import com.example.treba.simplecooking.data.simple_cooking_api_client.model.LocalUser;
import com.example.treba.simplecooking.data.simple_cooking_api_client.model.SimpleCookingError;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Authenticator;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;

/**
 * Created by treba on 19.01.2018.
 */

public class APIAuthenticator implements Authenticator {
    @Override
    public Request authenticate(Route route, Response response) throws IOException {
        Log.d("TAG","retry");

        if(responseCount(response)>=3){
            return null;
        }
        JSONObject json =SimpleCookingAPI.getInstance().refreshAccessToken();
        Log.d("TAG","JSON: "+json.toString());
        SimpleCookingError error = ErrorParser.parse(json);
        if(error!=null)return null;
        LocalUser localUser= LocalUserParser.parse(json);
        if(localUser==null)return null;
        String access_token = localUser.getAccessToken();
        SimpleCookingAPI.localUser=localUser;
        Log.d("TAG","returning response");
        return response.request().newBuilder().removeHeader("access_token").addHeader("access_token",localUser.getAccessToken()).build();


    }

    private int responseCount(Response response) {
        int result = 1;
        while ((response = response.priorResponse()) != null) {
            result++;
        }
        return result;
    }
}
