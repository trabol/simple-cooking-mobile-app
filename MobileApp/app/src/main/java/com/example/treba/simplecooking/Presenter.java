package com.example.treba.simplecooking;

import android.app.Fragment;
import android.content.Context;

/**
 * Created by treba on 20.02.2018.
 */

public class Presenter {
    protected Context context;
    protected Fragment fragment;

    public Presenter(Context context) {
        this.context = context;
    }

    public Presenter(Context context, Fragment fragment) {

        this.context = context;
        this.fragment = fragment;
    }

    public Fragment getFragment() {
        return fragment;
    }

    public void setFragment(Fragment fragment) {
        this.fragment = fragment;
    }
}
