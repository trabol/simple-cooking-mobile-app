package com.example.treba.simplecooking.data.simple_cooking_api_client.model;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by treba on 14.01.2018.
 */

public class LocalUser extends User implements Parcelable {
    private String accessToken;
    private String refreshToken;

    public LocalUser(long id, String name, String email, Uri picture, String accessToken, String refreshToken) {
        super(id, name, email, picture);
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
    }

    public LocalUser(long id, String name, String email, Uri picture, String refreshToken) {
        super(id, name, email, picture);
        this.accessToken = null;
        this.refreshToken = refreshToken;
    }

    public LocalUser(){
        this.accessToken=null;
        this.refreshToken=null;
    }


    protected LocalUser(Parcel in) {
        super(in);
        accessToken = in.readString();
        refreshToken = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(accessToken);
        dest.writeString(refreshToken);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LocalUser> CREATOR = new Creator<LocalUser>() {
        @Override
        public LocalUser createFromParcel(Parcel in) {
            return new LocalUser(in);
        }

        @Override
        public LocalUser[] newArray(int size) {
            return new LocalUser[size];
        }
    };

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    @Override
    public String toString() {
        return super.toString()+" LocalUser{" +
                "accessToken='" + accessToken + '\'' +
                ", refreshToken='" + refreshToken + '\'' +
                '}';
    }

}
