package com.example.treba.simplecooking.data.local_storage;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.treba.simplecooking.data.local_storage.database.SimpleCookingDbHelper;
import com.example.treba.simplecooking.data.local_storage.shared_preferences.UserSessionManager;
import com.example.treba.simplecooking.data.simple_cooking_api_client.model.LocalUser;
import com.example.treba.simplecooking.data.simple_cooking_api_client.service.SimplceCookingResultRecevier;

/**
 * Created by treba on 15.02.2018.
 */

public class SimpleCookingLocalStorageService extends IntentService {
    private SimpleCookingDbHelper dbHelper;
    public SimpleCookingLocalStorageService() {
        super("SimpleCookingLocalStorageService");
    }
    public static final String ACTION_GET_CURRENT_USER="get-curent-user";
    public static final String ACTION_SET_CURRENT_USER="set-curent-user";
    public static final String PARAM_CALLBACK="callback";
    public static final String EXTRA_ID="id";
    public static final String EXTRA_NAME="name";
    public static final String EXTRA_INGREDIENT="ingredient";
    public static final String EXTRA_AUTH_CODE = "auth-code";
    public static final String EXTRA_DISH="dish";
    public static final String EXTRA_INGREDIENTS="ingredients";
    public static final String EXTRA_USER="user";

    private UserSessionManager userManager;

    @Override
    public void onCreate() {
        super.onCreate();
        dbHelper= new SimpleCookingDbHelper(getApplicationContext());
        userManager=new UserSessionManager(getApplicationContext());
        Log.d("TAG","service created");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Log.d("TAG","service started");
        String action=intent.getAction();
        ResultReceiver recevier= intent.getParcelableExtra(PARAM_CALLBACK);
        if(action.equals(ACTION_GET_CURRENT_USER)){
            getCurentUser(intent,recevier);
        }else if(action.equals(ACTION_SET_CURRENT_USER)){
            setCurrentUser(intent,recevier);
        }
    }


    public void getCurentUser(Intent intent,ResultReceiver recevier){
        Bundle bundle = new Bundle();
        int code= SimplceCookingResultRecevier.RESULT_OK;
        LocalUser user = userManager.getCurentUser();
        bundle.putParcelable(SimplceCookingResultRecevier.PARAM_RESULT,user);
        if(recevier!=null){
            recevier.send(code,bundle);
        }
    }

    public void setCurrentUser(Intent intent,ResultReceiver recevier){
        Log.d("TAG","setting user");
        Bundle bundle = new Bundle();
        int code= SimplceCookingResultRecevier.RESULT_OK;
        LocalUser user = intent.getParcelableExtra(EXTRA_USER);
        Log.d("TAG","parceled "+user.toString());
        userManager.setCurrentUser(user);
        Log.d("TAG","user setted");
        LocalUser sendedUser=userManager.getCurentUser();
        bundle.putParcelable(SimplceCookingResultRecevier.PARAM_RESULT,sendedUser);
        Log.d("TAG",sendedUser.toString());

        if(recevier!=null){
            Log.d("TAG","sending tp rec");
            recevier.send(code,bundle);
        }
    }
}
