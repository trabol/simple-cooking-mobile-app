package com.example.treba.simplecooking.data.simple_cooking_api_client.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.treba.simplecooking.data.simple_cooking_api_client.API.SimpleCookingAPI;
import com.example.treba.simplecooking.data.simple_cooking_api_client.API.parser.DishParser;
import com.example.treba.simplecooking.data.simple_cooking_api_client.API.parser.ErrorParser;
import com.example.treba.simplecooking.data.simple_cooking_api_client.API.parser.IngredientParser;
import com.example.treba.simplecooking.data.simple_cooking_api_client.API.parser.LocalUserParser;
import com.example.treba.simplecooking.data.simple_cooking_api_client.API.parser.UserParser;
import com.example.treba.simplecooking.data.simple_cooking_api_client.model.Dish;
import com.example.treba.simplecooking.data.simple_cooking_api_client.model.Ingredient;
import com.example.treba.simplecooking.data.simple_cooking_api_client.model.LocalUser;
import com.example.treba.simplecooking.data.simple_cooking_api_client.model.SimpleCookingError;
import com.example.treba.simplecooking.data.simple_cooking_api_client.model.User;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by treba on 19.01.2018.
 */

public class SimpleCookingAPIService extends IntentService {

    public SimpleCookingAPIService() {
        super("SimpleCooking");
    }
    public static final String ACTION_GET_USER_BY_ID="get-user-by-id";
    public static final String ACTION_GET_ACCESS_TOKEN="get-access-token";
    public static final String ACTION_AUTHENTICATE_WITH_GOOGLE="action-authenticate-with-google";
    public static final String ACTION_GET_INGREDIENT_BY_ID="get-ingredient-by-id";
    public static final String ACTION_POST_INGREDIENT="post-ingredient";
    public static final String ACTION_GET_INGREDIENT_BY_NAME="get-ingredient-by-name";
    public static final String ACTION_GET_DISH_BY_ID="get-dish-by-id";
    public static final String ACTION_POST_DISH="post-dish";
    public static final String ACTION_UPDATE_DISH="update-dish";
    public static final String ACTION_DELETE_DISH="delete-dish";
    public static final String ACTION_SIMPLE_SEARCH="simple-search";
    public static final String PARAM_CALLBACK="callback";
    public static final String EXTRA_ID="id";
    public static final String EXTRA_NAME="name";
    public static final String EXTRA_INGREDIENT="ingredient";
    public static final String EXTRA_AUTH_CODE = "auth-code";
    public static final String EXTRA_DISH="dish";
    public static final String EXTRA_INGREDIENTS="ingredients";

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Log.d("TAG","service starteddd");
        String action=intent.getAction();
        ResultReceiver recevier= intent.getParcelableExtra(PARAM_CALLBACK);
        Log.d("TAG","recevier decoded");
        if(action.equals(ACTION_GET_USER_BY_ID)){
            getUserById(intent,recevier);
        }else if(action.equals(ACTION_GET_ACCESS_TOKEN)){

        }else if(action.equals(ACTION_AUTHENTICATE_WITH_GOOGLE)){
            authentiacteWithGoogle(intent,recevier);
        }else if(action.equals(ACTION_GET_INGREDIENT_BY_ID)){

        }else if(action.equals(ACTION_POST_INGREDIENT)){
            postIngredient(intent,recevier);
        }else if(action.equals(ACTION_GET_INGREDIENT_BY_NAME)){
            getIngredientByName(intent,recevier);
        }else if(action.equals(ACTION_GET_DISH_BY_ID)){

        }else if(action.equals(ACTION_POST_DISH)){
            postDish(intent,recevier);
        }else if(action.equals(ACTION_UPDATE_DISH)){

        }else if(action.equals(ACTION_DELETE_DISH)){

        }else if(action.equals(ACTION_SIMPLE_SEARCH)){
            simpleSearch(intent,recevier);
        }
    }


    private void getUserById(Intent intent,ResultReceiver recevier){
        Bundle bundle = new Bundle();
        int code;
        long id = intent.getLongExtra(EXTRA_ID,0);
        JSONObject json =SimpleCookingAPI.getInstance().getUserById(id);
        SimpleCookingError error = ErrorParser.parse(json);
        if(error!=null){
            code=SimplceCookingResultRecevier.RESULT_EROOR;
            bundle.putParcelable(SimplceCookingResultRecevier.PARAM_ERROR,error);
        }else{
            User user= UserParser.parse(json);
            if(user!=null){
                code=SimplceCookingResultRecevier.RESULT_OK;
                bundle.putParcelable(SimplceCookingResultRecevier.PARAM_RESULT,user);
            }else{
                code=SimplceCookingResultRecevier.RESULT_EROOR;
                bundle.putParcelable(SimplceCookingResultRecevier.PARAM_ERROR, SimpleCookingError.ParseError);
            }
        }
        if(recevier!=null){
            recevier.send(code,bundle);
        }
    }

    private void refreshAccessToken(Intent intent,ResultReceiver recevier){
        Bundle bundle = new Bundle();
        int code;
        JSONObject json =SimpleCookingAPI.getInstance().refreshAccessToken();
        SimpleCookingError error = ErrorParser.parse(json);
        if(error!=null){
            code=SimplceCookingResultRecevier.RESULT_EROOR;
            bundle.putParcelable(SimplceCookingResultRecevier.PARAM_ERROR,error);
        }else{
            LocalUser localUser= LocalUserParser.parse(json);
            if(localUser!=null){
                code=SimplceCookingResultRecevier.RESULT_OK;
                bundle.putParcelable(SimplceCookingResultRecevier.PARAM_RESULT,localUser);
            }else{
                code=SimplceCookingResultRecevier.RESULT_EROOR;
                bundle.putParcelable(SimplceCookingResultRecevier.PARAM_ERROR, SimpleCookingError.ParseError);
            }
        }
        if(recevier!=null){
            recevier.send(code,bundle);
        }
    }

    private void authentiacteWithGoogle(Intent intent,ResultReceiver recevier){
        Bundle bundle = new Bundle();
        int code;
        String authCode = intent.getStringExtra(EXTRA_AUTH_CODE);
        Log.d("TAG,","receving details");
        JSONObject json =SimpleCookingAPI.getInstance().authenticateWithGoogle(authCode);
        Log.d("TAG,","reced details");
        Log.d("TAG,",json.toString());
        SimpleCookingError error = ErrorParser.parse(json);
        if(error!=null){
            Log.d("TAG,","error receving details");
            code=SimplceCookingResultRecevier.RESULT_EROOR;
            bundle.putParcelable(SimplceCookingResultRecevier.PARAM_ERROR,error);
        }else{
            LocalUser localUser= LocalUserParser.parse(json);
            if(localUser!=null){
                code=SimplceCookingResultRecevier.RESULT_OK;
                bundle.putParcelable(SimplceCookingResultRecevier.PARAM_RESULT,localUser);
            }else{
                code=SimplceCookingResultRecevier.RESULT_EROOR;
                bundle.putParcelable(SimplceCookingResultRecevier.PARAM_ERROR, SimpleCookingError.ParseError);
            }
        }
        if(recevier!=null){
            recevier.send(code,bundle);
        }
    }

    private void getIngredientById(Intent intent,ResultReceiver recevier){
        Bundle bundle = new Bundle();
        int code;
        long id = intent.getLongExtra(EXTRA_ID,0);
        JSONObject json =SimpleCookingAPI.getInstance().getIngredientById(id);
        SimpleCookingError error = ErrorParser.parse(json);
        if(error!=null){
            code=SimplceCookingResultRecevier.RESULT_EROOR;
            bundle.putParcelable(SimplceCookingResultRecevier.PARAM_ERROR,error);
        }else{
            Ingredient ingredient=IngredientParser.parse(json);
            if(ingredient!=null){
                code=SimplceCookingResultRecevier.RESULT_OK;
                bundle.putParcelable(SimplceCookingResultRecevier.PARAM_RESULT,ingredient);
            }else{
                code=SimplceCookingResultRecevier.RESULT_EROOR;
                bundle.putParcelable(SimplceCookingResultRecevier.PARAM_ERROR, SimpleCookingError.ParseError);
            }
        }
        if(recevier!=null){
            recevier.send(code,bundle);
        }
    }

    private void postIngredient(Intent intent, ResultReceiver recevier){
        Bundle bundle = new Bundle();
        int code;
        Ingredient ingredient = intent.getParcelableExtra(EXTRA_INGREDIENT);
        JSONObject json=SimpleCookingAPI.getInstance().postIngredient(ingredient);
        SimpleCookingError error= ErrorParser.parse(json);
        if(error!=null){
            code=SimplceCookingResultRecevier.RESULT_EROOR;
            bundle.putParcelable(SimplceCookingResultRecevier.PARAM_ERROR,error);
        }else{
            Ingredient ingredientOUT= IngredientParser.parse(json);
            if(ingredientOUT!=null) {
                code=SimplceCookingResultRecevier.RESULT_OK;
                bundle.putParcelable(SimplceCookingResultRecevier.PARAM_RESULT, ingredientOUT);
            }else {
                code=SimplceCookingResultRecevier.RESULT_EROOR;
                bundle.putParcelable(SimplceCookingResultRecevier.PARAM_ERROR, SimpleCookingError.ParseError);
            }
        }
        if(recevier!=null){
            recevier.send(code,bundle);
        }
    }

    private void getIngredientByName(Intent intent,ResultReceiver recevier){
        Log.d("TAG","get ing by name service");
        Bundle bundle = new Bundle();
        int code;
        String name = intent.getStringExtra(EXTRA_NAME);
        JSONObject json=SimpleCookingAPI.getInstance().getIngredientByName(name);
        SimpleCookingError error= ErrorParser.parse(json);
        if(error!=null){
            code=SimplceCookingResultRecevier.RESULT_EROOR;
            bundle.putParcelable(SimplceCookingResultRecevier.PARAM_ERROR,error);
        }else{
            Ingredient ingredient= IngredientParser.parse(json);
            if(ingredient!=null) {
                code=SimplceCookingResultRecevier.RESULT_OK;
                bundle.putParcelable(SimplceCookingResultRecevier.PARAM_RESULT, ingredient);
            }else {
                code=SimplceCookingResultRecevier.RESULT_EROOR;
                bundle.putParcelable(SimplceCookingResultRecevier.PARAM_ERROR, SimpleCookingError.ParseError);
            }
        }
        if(recevier!=null){
            recevier.send(code,bundle);
        }
    }

    private void getDishById(Intent intent,ResultReceiver recevier){
        Bundle bundle = new Bundle();
        int code;
        long id = intent.getLongExtra(EXTRA_ID,0);
        JSONObject json =SimpleCookingAPI.getInstance().getDishById(id);
        SimpleCookingError error = ErrorParser.parse(json);
        if(error!=null){
            code=SimplceCookingResultRecevier.RESULT_EROOR;
            bundle.putParcelable(SimplceCookingResultRecevier.PARAM_ERROR,error);
        }else{
            Dish dish = DishParser.parse(json);
            if(dish!=null){
                code=SimplceCookingResultRecevier.RESULT_OK;
                bundle.putParcelable(SimplceCookingResultRecevier.PARAM_RESULT,dish);
            }else{
                code=SimplceCookingResultRecevier.RESULT_EROOR;
                bundle.putParcelable(SimplceCookingResultRecevier.PARAM_ERROR, SimpleCookingError.ParseError);
            }
        }
        if(recevier!=null){
            recevier.send(code,bundle);
        }
    }

    private void postDish(Intent intent,ResultReceiver recevier){
        Bundle bundle = new Bundle();
        int code;
        Log.d("TAG","service before dish");
        Dish postedDish = intent.getParcelableExtra(EXTRA_DISH);
        Log.d("TAG","after dish started");
        JSONObject json =SimpleCookingAPI.getInstance().postDish(postedDish);
        Log.d("TAG",json.toString());
        SimpleCookingError error = ErrorParser.parse(json);
        if(error!=null){
            code=SimplceCookingResultRecevier.RESULT_EROOR;
            bundle.putParcelable(SimplceCookingResultRecevier.PARAM_ERROR,error);
        }else{
            Dish dish = DishParser.parse(json);
            if(dish!=null){
                code=SimplceCookingResultRecevier.RESULT_OK;
                bundle.putParcelable(SimplceCookingResultRecevier.PARAM_RESULT,dish);
            }else{
                code=SimplceCookingResultRecevier.RESULT_EROOR;
                bundle.putParcelable(SimplceCookingResultRecevier.PARAM_ERROR, SimpleCookingError.ParseError);
            }
        }
        if(recevier!=null){
            recevier.send(code,bundle);
        }
    }

    private void updateDish(Intent intent,ResultReceiver recevier){
        Bundle bundle = new Bundle();
        int code;
        Dish updatedDish = intent.getParcelableExtra(EXTRA_DISH);
        JSONObject json =SimpleCookingAPI.getInstance().updateDish(updatedDish);
        SimpleCookingError error = ErrorParser.parse(json);
        if(error!=null){
            code=SimplceCookingResultRecevier.RESULT_EROOR;
            bundle.putParcelable(SimplceCookingResultRecevier.PARAM_ERROR,error);
        }else{
            Dish dish = DishParser.parse(json);
            if(dish!=null){
                code=SimplceCookingResultRecevier.RESULT_OK;
                bundle.putParcelable(SimplceCookingResultRecevier.PARAM_RESULT,dish);
            }else{
                code=SimplceCookingResultRecevier.RESULT_EROOR;
                bundle.putParcelable(SimplceCookingResultRecevier.PARAM_ERROR, SimpleCookingError.ParseError);
            }
        }
        if(recevier!=null){
            recevier.send(code,bundle);
        }
    }

    private void deletDish(Intent intent,ResultReceiver recevier){
        Bundle bundle = new Bundle();
        int code;
        long id = intent.getLongExtra(EXTRA_ID,0);
        JSONObject json =SimpleCookingAPI.getInstance().deleteDish(id);
        SimpleCookingError error = ErrorParser.parse(json);
        if(error!=null){
            code=SimplceCookingResultRecevier.RESULT_EROOR;
            bundle.putParcelable(SimplceCookingResultRecevier.PARAM_ERROR,error);
        }else{
            Dish dish = DishParser.parse(json);
            if(dish!=null){
                code=SimplceCookingResultRecevier.RESULT_OK;
                bundle.putParcelable(SimplceCookingResultRecevier.PARAM_RESULT,dish);
            }else{
                code=SimplceCookingResultRecevier.RESULT_EROOR;
                bundle.putParcelable(SimplceCookingResultRecevier.PARAM_ERROR, SimpleCookingError.ParseError);
            }
        }
        if(recevier!=null){
            recevier.send(code,bundle);
        }
    }

    private void simpleSearch(Intent intent,ResultReceiver recevier){
        Log.d("TAG","service switched");
        Bundle bundle = new Bundle();
        int code;
        ArrayList<Ingredient> searchedIngredients=intent.getParcelableArrayListExtra(EXTRA_INGREDIENTS);
        JSONObject json =SimpleCookingAPI.getInstance().simpleSearch(searchedIngredients);
        Log.d("TAG","Request got");
        SimpleCookingError error = ErrorParser.parse(json);
        if(error!=null){
            code=SimplceCookingResultRecevier.RESULT_EROOR;
            bundle.putParcelable(SimplceCookingResultRecevier.PARAM_ERROR,error);
        }else{
            ArrayList<Dish> dishes = DishParser.parseToList(json);
            if(dishes!=null){
                code=SimplceCookingResultRecevier.RESULT_OK_ARRAY_LIST;
                bundle.putParcelableArrayList(SimplceCookingResultRecevier.PARAM_RESULT, dishes);
            }else{
                code=SimplceCookingResultRecevier.RESULT_EROOR;
                bundle.putParcelable(SimplceCookingResultRecevier.PARAM_ERROR, SimpleCookingError.ParseError);
            }
        }
        if(recevier!=null){
            Log.d("TAG","sending signal to recevier");
            recevier.send(code,bundle);
        }
    }
}
