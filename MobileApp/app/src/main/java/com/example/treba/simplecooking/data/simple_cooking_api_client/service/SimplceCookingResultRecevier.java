package com.example.treba.simplecooking.data.simple_cooking_api_client.service;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

import com.example.treba.simplecooking.data.simple_cooking_api_client.model.SimpleCookingError;

/**
 * Created by treba on 19.01.2018.
 */

public class SimplceCookingResultRecevier<T> extends ResultReceiver {
    ResultRecevierCallback mCallback;
    public static int RESULT_OK=100;
    public static int RESULT_OK_ARRAY=101;
    public static int RESULT_OK_ARRAY_LIST=102;
    public static int RESULT_EROOR=2;
    public static String PARAM_RESULT="RESULT";
    public static String PARAM_ERROR="ERROR";
    public SimplceCookingResultRecevier(Handler handler) {
        super(handler);
    }

    public void setCallback(ResultRecevierCallback<T> callback){
        mCallback=callback;
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        if(mCallback==null)return;

        if(resultCode==RESULT_OK){
            mCallback.onSuccess(resultData.getParcelable(PARAM_RESULT));
        }else if(resultCode==RESULT_OK_ARRAY){
            mCallback.onSuccess(resultData.getParcelableArray(PARAM_RESULT));
        }else if(resultCode==RESULT_OK_ARRAY_LIST){
            mCallback.onSuccess(resultData.getParcelableArrayList(PARAM_RESULT));
        }else{
            mCallback.onError((SimpleCookingError)resultData.getParcelable(PARAM_ERROR));
        }
    }
}
