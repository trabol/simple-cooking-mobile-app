package com.example.treba.simplecooking.utils;

import android.app.Application;
import android.text.TextUtils;


/**
 * Created by treba on 14.01.2018.
 */

public class ApplicationController extends Application {


    private static ApplicationController sInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance=this;
    }

    public static synchronized ApplicationController getsInstance(){
        return sInstance;
    }
}
