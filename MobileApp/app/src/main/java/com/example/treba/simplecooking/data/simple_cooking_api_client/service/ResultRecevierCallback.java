package com.example.treba.simplecooking.data.simple_cooking_api_client.service;

import com.example.treba.simplecooking.data.simple_cooking_api_client.model.SimpleCookingError;

/**
 * Created by treba on 19.01.2018.
 */

public class ResultRecevierCallback<T> {
    public void onSuccess(T data){

    };
    public void onError(SimpleCookingError error){

    };
}
