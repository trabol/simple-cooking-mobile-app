package com.example.treba.simplecooking.fragment.base;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.treba.simplecooking.IngredientAdapter;
import com.example.treba.simplecooking.MainActivity;
import com.example.treba.simplecooking.R;
import com.example.treba.simplecooking.data.simple_cooking_api_client.Client.SimpleCookingAPIClient;
import com.example.treba.simplecooking.data.simple_cooking_api_client.model.Dish;
import com.example.treba.simplecooking.data.simple_cooking_api_client.model.Ingredient;
import com.example.treba.simplecooking.data.simple_cooking_api_client.model.SimpleCookingError;
import com.example.treba.simplecooking.data.simple_cooking_api_client.service.ResultRecevierCallback;
import com.example.treba.simplecooking.fragment.SimpleSearchResultFragment;


import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by treba on 25.01.2018.
 */

public class SimpleSearchFragment extends Fragment implements IngredientAdapter.ClickHandler {

    private RecyclerView inputsContainer;
    private IngredientAdapter ingredientAdapter;
    private Button searchButton;
    private Button addButton;
    private EditText ingredientNameInput;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        ingredientAdapter=new IngredientAdapter(this);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_simple_search, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        searchButton = (Button) view.findViewById(R.id.search_button);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search(view);
            }
        });
        addButton=view.findViewById(R.id.add_ingredient);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tryToAddIngredient(view);
            }
        });
        ingredientNameInput=view.findViewById(R.id.input_ingredient_name);
        inputsContainer = (RecyclerView) view.findViewById(R.id.inputs_container);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        inputsContainer.setLayoutManager(layoutManager);
        inputsContainer.setHasFixedSize(true);
        inputsContainer.setAdapter(ingredientAdapter);
    }


    public void search(View view) {
        String message = "ingredients: ";
        ArrayList<Ingredient> ingredients = ingredientAdapter.getIngredients();
        for (Ingredient ingredient : ingredients) {
            message += "\n" + ingredient.getName();
        }
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
        Log.d("TAG", message);
        SimpleCookingAPIClient.getInstance().simpleSearch(getContext(), ingredients, new ResultRecevierCallback<ArrayList<Dish>>() {
            @Override
            public void onSuccess(final ArrayList<Dish> data) {

                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList(SimpleSearchResultFragment.EXTRA_DISHES, data);
                Log.d("TAG", bundle.getParcelableArrayList(SimpleSearchResultFragment.EXTRA_DISHES).toString());
                Fragment fragment = new SimpleSearchResultFragment();
                fragment.setArguments(bundle);

                MainActivity activity = (MainActivity) getActivity();
                activity.loadFragment(fragment);
            }

            @Override
            public void onError(SimpleCookingError error) {
                Log.d("TAG", error.getMessage());
            }
        });
    }

    private void addIngredient(Ingredient ingredient) {
        ingredientAdapter.addIngredient(ingredient);

    }

    public void tryToAddIngredient(View view) {
        String name = ingredientNameInput.getText().toString();
        SimpleCookingAPIClient.getInstance().getIngredientByName(getContext(),name,new GetIngredientByNameRecevier(this));
    }

    public void showError(SimpleCookingError error) {
        Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(Ingredient ingredient) {
        Toast.makeText(getContext(), ingredient.toString(), Toast.LENGTH_LONG).show();
    }

    private static class GetIngredientByNameRecevier extends ResultRecevierCallback<Ingredient> {
        private WeakReference<SimpleSearchFragment> activityRef;

        public GetIngredientByNameRecevier(SimpleSearchFragment fragment) {
            activityRef = new WeakReference(fragment);
        }

        @Override
        public void onSuccess(Ingredient ingredient) {
            Log.d("TAG", "Success");
            if (activityRef != null && activityRef.get() != null) {
                activityRef.get().addIngredient(ingredient);
            }
        }

        @Override
        public void onError(SimpleCookingError error) {

            if (activityRef != null && activityRef.get() != null) {
                activityRef.get().showError(error);
            }
        }
    }
}


