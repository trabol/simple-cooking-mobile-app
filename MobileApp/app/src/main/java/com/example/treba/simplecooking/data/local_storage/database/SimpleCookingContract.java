package com.example.treba.simplecooking.data.local_storage.database;

import android.provider.BaseColumns;

/**
 * Created by User on 01.02.2018.
 */

public class SimpleCookingContract {

    public class UserEntry implements BaseColumns {

        public static final String TABLE_NAME="users";

        public static final String  COLUMN_GLOBAL_ID="global_id";
        public static final String  COLUMN_NAME="name";
        public static final String  COLUMN_EMAIL="email";
        public static final String  COLUMN_PICTURE="picture";
        public static final String  COLUMN_REFRESH_TOKEN="refresh_token";
    }
}
