package com.example.treba.simplecooking;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.treba.simplecooking.data.local_storage.SimpleCookingLocalStorageClient;
import com.example.treba.simplecooking.data.local_storage.SimpleCookingLocalStorageService;
import com.example.treba.simplecooking.data.simple_cooking_api_client.Client.SimpleCookingAPIClient;
import com.example.treba.simplecooking.data.simple_cooking_api_client.model.Ingredient;
import com.example.treba.simplecooking.data.simple_cooking_api_client.model.LocalUser;
import com.example.treba.simplecooking.data.simple_cooking_api_client.model.SimpleCookingError;
import com.example.treba.simplecooking.data.simple_cooking_api_client.model.User;
import com.example.treba.simplecooking.data.simple_cooking_api_client.service.ResultRecevierCallback;
import com.example.treba.simplecooking.data.simple_cooking_api_client.service.SimplceCookingResultRecevier;
import com.example.treba.simplecooking.data.simple_cooking_api_client.service.SimpleCookingAPIService;
import com.example.treba.simplecooking.fragment.base.SimpleSearchFragment;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;

import java.lang.ref.WeakReference;

/**
 * Created by User on 31.01.2018.
 */

public class SignInFragment extends FragmentActivity implements GoogleApiClient.OnConnectionFailedListener,
View.OnClickListener{

    public static final String EXTRA_USER="user";

    private static final String WebAPIPublicKey="878826402052-vuv9orc3m91ujqb572c3gls5hkdjg6rk.apps.googleusercontent.com";

    private static final int RC_SIGN_IN = 9001;

    private GoogleApiClient mGoogleApiClient;

    private SignInButton mSignInButton;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestScopes(new Scope((Scopes.PLUS_LOGIN)),new Scope((Scopes.PLUS_ME)))
                .requestServerAuthCode(WebAPIPublicKey).build();
        mGoogleApiClient=new GoogleApiClient.Builder(this)
                .enableAutoManage(this,this)
                .addApi(Auth.GOOGLE_SIGN_IN_API,gso)
                .build();

        SignInButton mSignInButton =(SignInButton) findViewById(R.id.google_signin_button);
        mSignInButton.setSize(SignInButton.SIZE_WIDE);
        mSignInButton.setOnClickListener(this);

    }



    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.google_signin_button:
                signIn();
                break;

        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void signIn() {
        Log.d("TAG","SIGN IN");
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("TAG","RESULT");
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("TAG", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
            String oneTimeCode=acct.getServerAuthCode();
            getUserByCode(oneTimeCode);
        }else{
            Toast.makeText(this,"FAILED",Toast.LENGTH_LONG).show();
            Intent resultIntent = new Intent();
            setResult(Activity.RESULT_CANCELED, resultIntent);
            finish();
        }
    }

    private void getUserByCode(String code){
        Log.d("TAG", "getting user by code:");
        SimpleCookingAPIClient.getInstance().authenticateWithGoogle(getApplicationContext(),code,new GetUserByCodeRecevier(this));
    }

    private void saveUserLocally(LocalUser user){
        Log.d("TAG","slocal");
        SimpleCookingLocalStorageClient.getInstance().setCurrentUser(getApplicationContext(),user,new SaveUserLocallyRecevier(this));
    }
    private void showError(SimpleCookingError error){
        Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
        Intent resultIntent = new Intent();
        setResult(Activity.RESULT_CANCELED,resultIntent);
        finish();
    }

    private void finishLoggingIn(LocalUser user){
        Intent resultIntent = new Intent();
        resultIntent.putExtra(EXTRA_USER,user);
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }

    private static class GetUserByCodeRecevier extends ResultRecevierCallback<LocalUser> {
        private WeakReference<SignInFragment> activityRef;

        public GetUserByCodeRecevier(SignInFragment fragment) {
            activityRef = new WeakReference(fragment);
        }

        @Override
        public void onSuccess(LocalUser user) {
            Log.d("TAG", "Success");
            if (activityRef != null && activityRef.get() != null) {
                Log.d("TAG","ref");
                activityRef.get().saveUserLocally(user);
            }
        }

        @Override
        public void onError(SimpleCookingError error) {

            if (activityRef != null && activityRef.get() != null) {
                activityRef.get().showError(error);
            }
        }
    }

    private static class SaveUserLocallyRecevier extends ResultRecevierCallback<LocalUser>{
        private WeakReference<SignInFragment> activityRef;

        public SaveUserLocallyRecevier(SignInFragment fragment) {
            activityRef = new WeakReference(fragment);
        }

        @Override
        public void onSuccess(LocalUser user) {
            Log.d("TAG", "Success local");
            if (activityRef != null && activityRef.get() != null) {
                Log.d("TAG","ref");
                activityRef.get().finishLoggingIn(user);
            }
        }

        @Override
        public void onError(SimpleCookingError error) {

            if (activityRef != null && activityRef.get() != null) {
                activityRef.get().showError(error);
            }
        }
    }
}
