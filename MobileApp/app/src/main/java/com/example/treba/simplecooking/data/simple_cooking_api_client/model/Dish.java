package com.example.treba.simplecooking.data.simple_cooking_api_client.model;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by treba on 13.01.2018.
 */

public class Dish implements Parcelable {
    private long id;
    private ArrayList<Ingredient> ingredients;
    private String name;
    private String recipe;
    private Uri picture;//TODO add in api
    private long authorId;

    public Dish(long id, ArrayList<Ingredient> ingredients, String name, String recipe,long authorId) {
        this.id = id;
        this.ingredients = ingredients;
        this.name = name;
        this.recipe = recipe;
        this.authorId=authorId;
    }

    public Dish(ArrayList<Ingredient> ingredients, String name, String recipe) {
        this.id=-1;
        this.ingredients = ingredients;
        this.name = name;
        this.recipe = recipe;
        this.picture = null;
        this.authorId=-1;
    }

    public Dish() {
        this.id=-1;
        this.ingredients = new ArrayList<>();
        this.name = null;
        this.recipe = null;
        this.picture = null;
        this.authorId=-1;
    }


    protected Dish(Parcel in) {
        id = in.readLong();
        ingredients = in.createTypedArrayList(Ingredient.CREATOR);
        name = in.readString();
        recipe = in.readString();
        picture = in.readParcelable(Uri.class.getClassLoader());
        authorId = in.readLong();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeTypedList(ingredients);
        dest.writeString(name);
        dest.writeString(recipe);
        dest.writeParcelable(picture, flags);
        dest.writeLong(authorId);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Dish> CREATOR = new Creator<Dish>() {
        @Override
        public Dish createFromParcel(Parcel in) {
            return new Dish(in);
        }

        @Override
        public Dish[] newArray(int size) {
            return new Dish[size];
        }
    };

    public ArrayList<Ingredient> getIngredients() {
        return ingredients;
    }

    public String getName() {
        return name;
    }

    public String getRecipe() {
        return recipe;
    }

    public void setIngredients(ArrayList<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRecipe(String recipe) {
        this.recipe = recipe;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean addIngredient(Ingredient ingredient){
        for(Ingredient localIngredient:ingredients){
            if(localIngredient.getId()==ingredient.getId()){
                return false;
            }
        }
        ingredients.add(ingredient);
        return true;
    }

    public boolean deleteIngredinet(Ingredient ingredient){
        for(Ingredient localIngredient:ingredients){
            if(localIngredient.getId()==ingredient.getId()){
                ingredients.remove(localIngredient);
                return true;
            }
        }
        return false;
    }

    public long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(long authorId) {
        this.authorId = authorId;
    }

    @Override
    public String toString() {
        return "Dish{" +
                "id=" + id +
                ", ingredients=" + ingredients.toString() +
                ", name='" + name + '\'' +
                ", recipe='" + recipe + '\'' +
                '}';
    }


}
