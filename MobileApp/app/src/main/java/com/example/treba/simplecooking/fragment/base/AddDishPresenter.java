/*
package com.example.treba.simplecooking.fragment.base;

import android.app.Fragment;
import android.content.Context;

import com.example.treba.simplecooking.IngredientAdapter;
import com.example.treba.simplecooking.Presenter;
import com.example.treba.simplecooking.data.simple_cooking_api_client.Client.SimpleCookingAPIClient;
import com.example.treba.simplecooking.data.simple_cooking_api_client.model.Dish;

*/
/**
 * Created by treba on 20.02.2018.
 *//*


public class AddDishPresenter extends Presenter {

    private Dish dish;
    private IngredientAdapter adapter;

    public AddDishPresenter(Context context) {
        super(context);
        dish=new Dish();
    }

    public AddDishPresenter(Context context, Fragment fragment) {
        super(context, fragment);
    }

    public IngredientAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(IngredientAdapter adapter) {
        this.adapter = adapter;
    }

    public void updateName(String name){
        dish.setName(name);
    }

    public void updateRecipe(String receipt){
        dish.setRecipe(receipt);
    }

    //TODO public void updateImage()

    public void addDish(){
        SimpleCookingAPIClient.getInstance().postDish(this.context,dish,);
    }

}
*/
