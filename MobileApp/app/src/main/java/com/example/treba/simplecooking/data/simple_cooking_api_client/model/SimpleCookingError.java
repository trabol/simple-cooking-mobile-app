package com.example.treba.simplecooking.data.simple_cooking_api_client.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by treba on 19.01.2018.
 */

public class SimpleCookingError implements Parcelable {
    private long code;
    private long status;
    private String message;
    public static SimpleCookingError ServerNotResponding=new SimpleCookingError(0,0,"Server not responding");
    public static SimpleCookingError ParseError=new SimpleCookingError(0,0,"Parse Error");


    public SimpleCookingError(long code, long status, String message) {
        this.code = code;
        this.status = status;
        this.message = message;
    }

    protected SimpleCookingError(Parcel in) {
        code = in.readLong();
        status = in.readLong();
        message = in.readString();
    }

    public static final Creator<SimpleCookingError> CREATOR = new Creator<SimpleCookingError>() {
        @Override
        public SimpleCookingError createFromParcel(Parcel in) {
            return new SimpleCookingError(in);
        }

        @Override
        public SimpleCookingError[] newArray(int size) {
            return new SimpleCookingError[size];
        }
    };

    public long getCode() {
        return code;
    }

    public void setCode(long code) {
        this.code = code;
    }

    public long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(code);
        parcel.writeLong(status);
        parcel.writeString(message);
    }

    @Override
    public String toString() {
        return "SimpleCookingError{" +
                "code=" + code +
                ", status=" + status +
                ", message='" + message + '\'' +
                '}';
    }
}
