package com.example.treba.simplecooking.data.simple_cooking_api_client.model;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by treba on 13.01.2018.
 */

public class User implements Parcelable {
    private long id;
    private String name;
    private String email;
    private Uri picture;

    public User(long id, String name, String email, Uri picture) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.picture = picture;
    }

    public User(){
        this.id=0;
        this.name=null;
        this.email=null;
        this.picture=null;
    }

    protected User(Parcel in) {
        id = in.readLong();
        name = in.readString();
        email = in.readString();
        picture = in.readParcelable(Uri.class.getClassLoader());
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Uri getPicture() {
        return picture;
    }

    public void setPicture(Uri picture) {
        this.picture = picture;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", picture=" + picture +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeString(name);
        parcel.writeString(email);
        parcel.writeParcelable(picture, i);
    }
}
