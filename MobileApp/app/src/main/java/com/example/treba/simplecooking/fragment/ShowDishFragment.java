package com.example.treba.simplecooking.fragment;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.treba.simplecooking.R;
import com.example.treba.simplecooking.data.simple_cooking_api_client.API.parser.DishParser;
import com.example.treba.simplecooking.data.simple_cooking_api_client.model.Dish;
import com.example.treba.simplecooking.data.simple_cooking_api_client.model.Ingredient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by treba on 25.01.2018.
 */

public class ShowDishFragment extends Fragment {
    public static final String EXTRA_DISH="dish";
    private Dish dish;

    private TextView mNameTextView;
    private ImageView mPicture;
    private LinearLayout mInngredientsLayout;
    private TextView mRecipeTextView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args=getArguments();
        if(args!=null&&args.containsKey(EXTRA_DISH)){
            dish=args.getParcelable(EXTRA_DISH);
        }else{
            ArrayList<Ingredient> ingredients = new ArrayList<>();
            ingredients.add(new Ingredient("papaya"));
            ingredients.add(new Ingredient("apple"));
            //dish=new Dish(0,ingredients,"Something went wrong","If you see this dialog it means that there was a problem with obtaining your dish plase try again",0);
            try {
                dish= DishParser.parse(new JSONObject("{\n" +
                        "  \"dish\": {\n" +
                        "    \"id\": \"58\",\n" +
                        "    \"name\": \"Egg with Ketchup\",\n" +
                        "    \"recipe\": \"boil egg and add ketchup\",\n" +
                        "    \"ingredients\": [\n" +
                        "      {\n" +
                        "        \"id\": 14,\n" +
                        "        \"name\": \"egg\"\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"id\": 15,\n" +
                        "        \"name\": \"ketchup\"\n" +
                        "      }\n" +
                        "    ],\n" +
                        "    \"authorId\": 1\n" +
                        "  }\n" +
                        "}"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_show_dish, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mNameTextView=(TextView) view.findViewById(R.id.dish_name);
        mPicture =(ImageView) view.findViewById(R.id.dish_picture);
        mInngredientsLayout=(LinearLayout) view.findViewById(R.id.ingredients_layout);
        mRecipeTextView=(TextView) view.findViewById(R.id.dish_recipe);
        Log.d("TAG","relations set");
        fillDetails();
    }

    private void fillDetails(){
        mNameTextView.setText(dish.getName());
        //TODO dishes pictures
        mRecipeTextView.setText(dish.getRecipe());
        for(int i=0;i<dish.getIngredients().size();i++){
            Ingredient ingredient=dish.getIngredients().get(i);
            View ingredientView = this.getLayoutInflater().inflate(R.layout.ingredient_box,null);
            TextView ingredientTextView =ingredientView.findViewById(R.id.ingredient_name);
            ingredientTextView.setText(ingredient.getName());
            mInngredientsLayout.addView(ingredientView);
        }
    }
}
