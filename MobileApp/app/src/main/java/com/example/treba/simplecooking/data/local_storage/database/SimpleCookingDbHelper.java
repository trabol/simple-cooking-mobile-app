package com.example.treba.simplecooking.data.local_storage.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.example.treba.simplecooking.data.local_storage.database.SimpleCookingContract.UserEntry;

/**
 * Created by treba on 15.02.2018.
 */

public class SimpleCookingDbHelper extends SQLiteOpenHelper{

    public static final String DB_NAME="simple_cooking.db";
    public static final int DB_VERSION=1;

    public SimpleCookingDbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        final String CREATE_USER_TABLE=
                "CREATE TABLE " + UserEntry.TABLE_NAME + " (" +
                        UserEntry._ID               + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        UserEntry.COLUMN_GLOBAL_ID+ " INTEGER NOT NULL, "                 +
                        UserEntry.COLUMN_NAME+ " varchar(50) NOT NULL, "                 +

                        UserEntry.COLUMN_EMAIL + "varchar(60) NOT NULL,"                  +
                        UserEntry.COLUMN_PICTURE + "varchar(60) NOT NULL,"                  +
                        UserEntry.COLUMN_REFRESH_TOKEN + "varchar(100) NOT NULL,"                  +


//              COMPLETED (1) Add a UNIQUE constraint on the date column to replace on conflict
                /*
                 * To ensure this table can only contain one weather entry per date, we declare
                 * the date column to be unique. We also specify "ON CONFLICT REPLACE". This tells
                 * SQLite that if we have a weather entry for a certain date and we attempt to
                 * insert another weather entry with that date, we replace the old weather entry.
                 */
                        " UNIQUE (" + UserEntry.COLUMN_GLOBAL_ID + ") ON CONFLICT REPLACE);";

                    sqLiteDatabase.execSQL(CREATE_USER_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+UserEntry.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    public void getCurrentUser(){
    }
}
