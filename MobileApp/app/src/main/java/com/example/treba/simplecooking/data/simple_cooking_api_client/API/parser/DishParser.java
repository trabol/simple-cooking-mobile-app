package com.example.treba.simplecooking.data.simple_cooking_api_client.API.parser;

import android.net.Uri;

import com.example.treba.simplecooking.data.simple_cooking_api_client.model.Dish;
import com.example.treba.simplecooking.data.simple_cooking_api_client.model.Ingredient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import okhttp3.FormBody;

/**
 * Created by User on 21.01.2018.
 */

public class DishParser {
    public static  String DISH="dish";
    public static  String DISHES="dishes";
    public static String ID="id";
    public static String NAME="name";
    public static String INGREDIENT="ingredient";
    public static final String RECIPE="recipe";
    public static final String PICTURE="picture";
    public static final String AUTHOR_ID="authorId";

    public static Dish parse(JSONObject object) {
        if(object==null||!object.has(DISH)){
            return null;
        }
        try {
            JSONObject dish = object.getJSONObject(DISH);
            ArrayList<Ingredient> ingredients = new ArrayList<Ingredient>();
            return new Dish(dish.getLong(ID),IngredientParser.parseToList(dish),dish.getString(NAME),dish.getString(RECIPE),dish.getLong(AUTHOR_ID));
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static ArrayList<Dish> parseToList(JSONObject object){
        if(object==null||!object.has(DISHES)){
            return null;
        }
        ArrayList<Dish> dishes = new ArrayList<Dish>();
        try {
            JSONArray array = object.getJSONArray(DISHES);
            for(int i=0;i<array.length();i++){
                JSONObject dish = array.getJSONObject(i);
                dishes.add( new Dish(dish.getLong(ID),IngredientParser.parseToList(dish),dish.getString(NAME),dish.getString(RECIPE),dish.getLong(AUTHOR_ID)));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return dishes;
    }

    public static FormBody toFormBody(Dish dish){
        FormBody.Builder builder = new FormBody.Builder();
        builder.add(NAME,dish.getName())
                .add(RECIPE,dish.getRecipe());
        if(dish.getAuthorId()>0) {
            builder.add(AUTHOR_ID, String.valueOf(dish.getAuthorId()));
        }
        for (Ingredient ingredient:dish.getIngredients()) {
            builder.add(INGREDIENT,ingredient.getName());
        }
        return builder.build();
    }
}
