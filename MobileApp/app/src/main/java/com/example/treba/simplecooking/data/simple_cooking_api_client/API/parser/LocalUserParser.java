package com.example.treba.simplecooking.data.simple_cooking_api_client.API.parser;

import android.net.Uri;

import com.example.treba.simplecooking.data.simple_cooking_api_client.model.LocalUser;
import com.example.treba.simplecooking.data.simple_cooking_api_client.model.User;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by User on 20.01.2018.
 */

public class LocalUserParser{
        public static String USER="user";
        public static String ID="id";
        public static String NAME="name";
        public static String EMAIL="email";
        public static String PICTURE="picture";
        public static String REFRESH_TOKEN="refreshToken";
        public static String ACCESS_TOKEN="accessToken";

        public static LocalUser parse(JSONObject object) {
            if(object==null||!object.has(USER)){
                return null;
            }
            try {
                JSONObject user= object.getJSONObject(USER);
                return new LocalUser(user.getLong(ID),user.getString(NAME),user.getString(EMAIL),new Uri.Builder().path(user.getString(PICTURE)).build(),user.getString(ACCESS_TOKEN),user.getString(REFRESH_TOKEN));
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        }
}
