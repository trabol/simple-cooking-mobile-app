package com.example.treba.simplecooking.data.local_storage;


import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;

import com.example.treba.simplecooking.data.simple_cooking_api_client.model.LocalUser;
import com.example.treba.simplecooking.data.simple_cooking_api_client.model.User;
import com.example.treba.simplecooking.data.simple_cooking_api_client.service.ResultRecevierCallback;
import com.example.treba.simplecooking.data.simple_cooking_api_client.service.SimplceCookingResultRecevier;
import com.example.treba.simplecooking.data.simple_cooking_api_client.service.SimpleCookingAPIService;

/**
 * Created by treba on 17.02.2018.
 */

public class SimpleCookingLocalStorageClient {

    private static SimpleCookingLocalStorageClient mInstance;

    private SimpleCookingLocalStorageClient(){

    }
    public static SimpleCookingLocalStorageClient getInstance(){
        if(mInstance==null){
            mInstance=new SimpleCookingLocalStorageClient();
        }
        return mInstance;
    }

    public void getCurrentUser(Context context,final ResultRecevierCallback resultRecevierCallback){
        Intent intent =prepareIntent(context,resultRecevierCallback,SimpleCookingLocalStorageService.ACTION_GET_CURRENT_USER);
        context.startService(intent);
    }

    public void setCurrentUser(Context context,final LocalUser user, final ResultRecevierCallback resultRecevierCallback){
        Intent intent =prepareIntent(context,resultRecevierCallback,SimpleCookingLocalStorageService.ACTION_SET_CURRENT_USER);
        intent.putExtra(SimpleCookingLocalStorageService.EXTRA_USER,user);
        context.startService(intent);
    }

    private Intent prepareIntent(Context context,final ResultRecevierCallback resultRecevierCallback,String action){
        Intent intent = new Intent(context, SimpleCookingLocalStorageService.class);
        intent.setAction(action);
        SimplceCookingResultRecevier<LocalUser> recevier = new SimplceCookingResultRecevier<LocalUser>(new Handler());
        recevier.setCallback(resultRecevierCallback);
        intent.putExtra(SimpleCookingAPIService.PARAM_CALLBACK,recevier);
        return intent;
    }
}
