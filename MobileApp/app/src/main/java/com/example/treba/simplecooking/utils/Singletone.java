package com.example.treba.simplecooking.utils;

/**
 * Created by treba on 14.01.2018.
 */

public class Singletone {
    private static Singletone mInstance;

    public Singletone(){

    }

    public static Singletone getInstance(){
        if(mInstance==null)mInstance=new Singletone();
        return mInstance;
    }
}
