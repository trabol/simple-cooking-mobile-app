package com.example.treba.simplecooking.data.simple_cooking_api_client.API.parser;

import android.net.Uri;

import com.example.treba.simplecooking.data.simple_cooking_api_client.model.User;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by treba on 19.01.2018.
 */

public class UserParser {
    public static String USER="user";
    public static String ID="id";
    public static String NAME="name";
    public static String EMAIL="email";
    public static String PICTURE="picture";

    public static User parse(JSONObject object) {
        if(object==null||!object.has(USER)){
            return null;
        }
        try {
            JSONObject user= object.getJSONObject(USER);
            return new User(user.getLong(ID),user.getString(NAME),user.getString(EMAIL),new Uri.Builder().path(user.getString(PICTURE)).build());
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
