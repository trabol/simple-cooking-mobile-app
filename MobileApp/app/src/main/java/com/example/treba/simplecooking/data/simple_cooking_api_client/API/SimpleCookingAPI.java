package com.example.treba.simplecooking.data.simple_cooking_api_client.API;

import android.net.Uri;
import android.util.Log;

import com.example.treba.simplecooking.data.simple_cooking_api_client.API.parser.DishParser;
import com.example.treba.simplecooking.data.simple_cooking_api_client.API.parser.ErrorParser;
import com.example.treba.simplecooking.data.simple_cooking_api_client.API.parser.IngredientParser;
import com.example.treba.simplecooking.data.simple_cooking_api_client.model.Dish;
import com.example.treba.simplecooking.data.simple_cooking_api_client.model.Ingredient;
import com.example.treba.simplecooking.data.simple_cooking_api_client.model.LocalUser;
import com.example.treba.simplecooking.data.simple_cooking_api_client.model.SimpleCookingError;
import com.example.treba.simplecooking.data.simple_cooking_api_client.model.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;


import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by treba on 13.01.2018.
 */

public class SimpleCookingAPI {//use okhttp
    private static final String TAG=SimpleCookingAPI.class.getSimpleName();
    public static final String PROTOCOL = "https";
    public static final String HOST ="secure-river-46337.herokuapp.com";
    public static final String API_PATH="api";
    public static final Uri BASE_URI = new Uri.Builder().scheme(PROTOCOL).authority(HOST).appendPath(API_PATH).build();
    private static final Uri INGREDIENT_URI = BASE_URI.buildUpon().appendPath("ingredient").build();
    private static final Uri USER_URI = BASE_URI.buildUpon().appendPath("user").build();
    private static final Uri DISH_URI = BASE_URI.buildUpon().appendPath("dish").build();
    private static final Uri SEARCH_URI = BASE_URI.buildUpon().appendPath("search").build();
    private static final Uri SIMPLE_SEARCH_URI = SEARCH_URI.buildUpon().appendPath("simple").build();
    private static final Uri AUTHENTICATION_URI = BASE_URI.buildUpon().appendPath("auth").build();
    private static final Uri AUTHENTICATION_GOOGLE_URI = AUTHENTICATION_URI.buildUpon().appendPath("google").appendPath("callback").build();
    private static final Uri AUTHENTICATION_REFRESH_ACCESS_TOKEN_URI = AUTHENTICATION_URI.buildUpon().appendPath("refreshaccesstoken").build();

    private static final String CONTENT_TYPE="application/x-www-form-urlencoded";
    private static final String HEADER_ACCESS_TOKEN="access_token";


    public static LocalUser localUser=new LocalUser();

    private static OkHttpClient okHttpClient;

    private static SimpleCookingAPI mInstance;

    private SimpleCookingAPI(){

    }
    public static SimpleCookingAPI getInstance(){
        if(mInstance==null){
            mInstance=new SimpleCookingAPI();
            okHttpClient=new OkHttpClient.Builder().authenticator(new APIAuthenticator()).build();
        }
        return mInstance;
    }

    private void addHeaders(Request.Builder builder,boolean addAccessToken){
        builder.addHeader("Content-Type",CONTENT_TYPE);
        if(addAccessToken&&localUser.getAccessToken()!=null){
            Log.d(TAG,"setting access token "+localUser.getAccessToken());
            builder.addHeader(HEADER_ACCESS_TOKEN,localUser.getAccessToken());
        }
    }

   private JSONObject makeRequestForJSON(Request request) {
       try {
           Response response = okHttpClient.newCall(request).execute();
           String res = response.body().string();
           Log.d(TAG,res);
           return new JSONObject(res);
       } catch (IOException e) {
           Log.d("TAG","IOERROR");
           e.printStackTrace();
           return ErrorParser.toJSON(SimpleCookingError.ServerNotResponding);
       } catch (JSONException e) {
           Log.d("TAG","JSONERROR");
           e.printStackTrace();
           return ErrorParser.toJSON(SimpleCookingError.ServerNotResponding);
       }
   }

    public  JSONObject getUserById(long id) {
        Log.d(TAG,"getting user by id");
        Uri uri = USER_URI.buildUpon().appendPath(String.valueOf(id)).build();

        Request.Builder builder = new Request.Builder().
                url(uri.toString());
        addHeaders(builder,true);
        Request request = builder.build();
        return makeRequestForJSON(request);
    }

    private  JSONObject updateUser(User user) {//TODO and public
        Uri uri = USER_URI.buildUpon().appendPath(String.valueOf(user.getId())).build();

        Request.Builder builder = new Request.Builder().
                url(uri.toString());
        addHeaders(builder,true);
        Request request = builder.build();
        return makeRequestForJSON(request);
    }

    public  JSONObject refreshAccessToken(){
        Log.d(TAG,"Refreshing access token");
        Uri uri = AUTHENTICATION_REFRESH_ACCESS_TOKEN_URI.buildUpon().appendQueryParameter("refresh_token", String.valueOf(localUser.getRefreshToken())).build();
        Request.Builder builder = new Request.Builder().url(uri.toString());
        addHeaders(builder,false);
        Request request=builder.build();
        return makeRequestForJSON(request);
    }

    public  JSONObject authenticateWithGoogle(String oneTimeCode){
        Uri uri = AUTHENTICATION_GOOGLE_URI.buildUpon().appendQueryParameter("code", oneTimeCode).build();
        Request.Builder builder = new Request.Builder().
                url(uri.toString());
        addHeaders(builder,false);
        Request request = builder.build();
        return makeRequestForJSON(request);

    }

    public  JSONObject getIngredientById(long id) {
        Log.d(TAG,"getting ingredient by id");
        Uri uri = INGREDIENT_URI.buildUpon().appendPath( String.valueOf(id)).build();

        Request.Builder builder = new Request.Builder().
                url(uri.toString());
        addHeaders(builder,true);
        Request request = builder.build();
        return makeRequestForJSON(request);
    }

    public  JSONObject postIngredient(Ingredient ingredient) {
        Log.d(TAG, "posting ingredient");
        Uri uri = INGREDIENT_URI;
        Request.Builder builder = new Request.Builder().
                url(uri.toString());
        addHeaders(builder, true);
        RequestBody body = new FormBody.Builder()
                .add("name", ingredient.getName())
                .build();
        builder.post(body);
        Request request = builder.build();
        return makeRequestForJSON(request);
    }

    public  JSONObject updateIngrediont(Ingredient ingredient){
        //TODO
        return null;
    }

    public  JSONObject deleteIngrediont(long id){
        //TODO
        return null;
    }

    public  JSONObject getIngredientByName(String name) {
        Log.d(TAG,"getting ingredient by name");
        Uri uri = INGREDIENT_URI.buildUpon().appendPath("name").appendPath(name).build();
        Log.d("TAG",uri.toString());
        Request.Builder builder = new Request.Builder().
                url(uri.toString());
        addHeaders(builder,true);
        Request request = builder.build();
        return makeRequestForJSON(request);
    }

    public  JSONObject getDishById(long id){
        Uri uri = DISH_URI.buildUpon().appendPath( String.valueOf(id)).build();
        Log.d("TAG","requesting dish with id= "+uri.toString());
        Request.Builder builder = new Request.Builder().
                url(uri.toString());
        addHeaders(builder,true);
        Request request = builder.build();
        return makeRequestForJSON(request);


    }

    public  JSONObject postDish(final Dish dish){
        Uri uri = DISH_URI;
        Request.Builder builder = new Request.Builder().
                url(uri.toString());
        addHeaders(builder,true);
        builder.post(DishParser.toFormBody(dish));
        Request request = builder.build();
        return makeRequestForJSON(request);
    }

    public  JSONObject updateDish(final Dish dish){
        Uri uri = DISH_URI.buildUpon().appendPath(String.valueOf(dish.getId())).build();
        Request.Builder builder = new Request.Builder().
                url(uri.toString());
        addHeaders(builder,true);
        builder.put(DishParser.toFormBody(dish));
        Request request = builder.build();
        return makeRequestForJSON(request);
    }

    public  JSONObject deleteDish(final long id){
        Uri uri = DISH_URI.buildUpon().appendPath( String.valueOf(id)).build();
        Request.Builder builder = new Request.Builder().
                url(uri.toString());
        addHeaders(builder,true);
        builder.delete();
        Request request = builder.build();
        return makeRequestForJSON(request);
    }

    public JSONObject simpleSearch(List<Ingredient> ingredients){
        Log.d("TAG","Searching simple");
        Uri.Builder uriBuilder = SIMPLE_SEARCH_URI.buildUpon();
        for(Ingredient ingredient:ingredients) {
            uriBuilder.appendQueryParameter(IngredientParser.INGREDIENT,ingredient.getName());
        }
        Uri uri = uriBuilder.build();
        Log.d("TAG",uri.toString());
        Request.Builder builder = new Request.Builder().
                url(uri.toString());
        addHeaders(builder,true);
        Request request = builder.build();
        return makeRequestForJSON(request);
    }

}
