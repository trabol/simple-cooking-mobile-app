package com.example.treba.simplecooking.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.treba.simplecooking.DishAdapter;
import com.example.treba.simplecooking.MainActivity;
import com.example.treba.simplecooking.R;
import com.example.treba.simplecooking.data.simple_cooking_api_client.model.Dish;

/**
 * Created by treba on 26.01.2018.
 */

public class SimpleSearchResultFragment extends Fragment implements DishAdapter.ClickHandler {

    public static final String EXTRA_DISHES="dishes";
    private RecyclerView mRecyclerView;
    private DishAdapter dishAdapter;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_simple_search_result, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        dishAdapter = new DishAdapter(this);
        Bundle args = getArguments();
        if (args != null && args.containsKey(EXTRA_DISHES)) {
            dishAdapter.setDishes(args.<Dish>getParcelableArrayList(EXTRA_DISHES));
        } else {
            Log.d("TAG", "no extra dishes");
        }
        mRecyclerView =(RecyclerView)view.findViewById(R.id.dish_recycler_view);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(dishAdapter);
    }


    @Override
    public void onClick(Dish dish) {
        MainActivity activity = (MainActivity) getActivity();
        Fragment fragment = new ShowDishFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(ShowDishFragment.EXTRA_DISH,dish);
        fragment.setArguments(bundle);
        activity.loadFragment(fragment);
    }
}
