package com.example.treba.simplecooking.fragment.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.treba.simplecooking.IngredientAdapter;
import com.example.treba.simplecooking.MainActivity;
import com.example.treba.simplecooking.R;
import com.example.treba.simplecooking.data.simple_cooking_api_client.API.parser.DishParser;
import com.example.treba.simplecooking.data.simple_cooking_api_client.Client.SimpleCookingAPIClient;
import com.example.treba.simplecooking.data.simple_cooking_api_client.model.Dish;
import com.example.treba.simplecooking.data.simple_cooking_api_client.model.Ingredient;
import com.example.treba.simplecooking.data.simple_cooking_api_client.model.LocalUser;
import com.example.treba.simplecooking.data.simple_cooking_api_client.model.SimpleCookingError;
import com.example.treba.simplecooking.data.simple_cooking_api_client.service.ResultRecevierCallback;
import com.example.treba.simplecooking.fragment.ShowDishFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;


/**
 * Created by treba on 25.01.2018.
 */

public class AddDishFragment extends Fragment implements IngredientAdapter.ClickHandler {

    private RecyclerView inputsContainer;
    private TextView mNameTextView;
    private ImageView mPicture;
    private LinearLayout mInngredientsLayout;
    private TextView mRecipeTextView;

    private Button addIngredientButton;
    private EditText ingredientNameInput;
    private Button addDishButton;

    private Dish dish;
    private IngredientAdapter ingredientAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dish=new Dish();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_dish, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mNameTextView=(TextView) view.findViewById(R.id.dish_name);
        mPicture =(ImageView) view.findViewById(R.id.dish_picture);
        mInngredientsLayout=(LinearLayout) view.findViewById(R.id.ingredients_layout);
        mRecipeTextView=(TextView) view.findViewById(R.id.dish_recipe);
        addIngredientButton=view.findViewById(R.id.add_ingredient);
        addIngredientButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tryToAddIngredient(view);
            }
        });
        addDishButton=view.findViewById(R.id.add_dish);
        addDishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addDish();
            }
        });
        ingredientNameInput=view.findViewById(R.id.input_ingredient_name);

        ingredientAdapter=new IngredientAdapter(this);
        inputsContainer = (RecyclerView) view.findViewById(R.id.inputs_container);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        inputsContainer.setLayoutManager(layoutManager);
        inputsContainer.setHasFixedSize(true);
        inputsContainer.setAdapter(ingredientAdapter);

    }

    private void addIngredient(Ingredient ingredient) {
        ingredientAdapter.addIngredient(ingredient);

    }

    public void tryToAddIngredient(View view) {
        Log.d("TAG","Trying to add ingredient");
        String name = ingredientNameInput.getText().toString();
        SimpleCookingAPIClient.getInstance().getIngredientByName(getContext(),name,new AddDishFragment.GetIngredientByNameRecevier(this));
    }



    public void addDish(){
        dish.setName(mNameTextView.getText().toString());
        dish.setIngredients(ingredientAdapter.getIngredients());
        Log.d("TAG",dish.getIngredients().size()+"|||\n"+dish.getIngredients().toString());
        dish.setRecipe(mRecipeTextView.getText().toString());
        SimpleCookingAPIClient.getInstance().postDish(getContext(),dish,new AddDishRecevier(this));
    }

    public void onDishAdded(Dish receviedDish){
        MainActivity activity = (MainActivity) getActivity();
        Fragment fragment = new ShowDishFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(ShowDishFragment.EXTRA_DISH,dish);
        fragment.setArguments(bundle);
        activity.loadFragment(fragment);
    }

    public void showError(SimpleCookingError error){
        Toast.makeText(getContext(),error.getMessage(),Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(Ingredient ingredient) {
        Toast.makeText(getContext(), ingredient.toString(), Toast.LENGTH_LONG).show();
    }

    private static class GetIngredientByNameRecevier extends ResultRecevierCallback<Ingredient> {
        private WeakReference<AddDishFragment> activityRef;

        public GetIngredientByNameRecevier(AddDishFragment fragment) {
            activityRef = new WeakReference(fragment);
        }

        @Override
        public void onSuccess(Ingredient ingredient) {
            Log.d("TAG", "Success");
            if (activityRef != null && activityRef.get() != null) {
                activityRef.get().addIngredient(ingredient);
            }
        }

        @Override
        public void onError(SimpleCookingError error) {

            if (activityRef != null && activityRef.get() != null) {
                activityRef.get().showError(error);
            }
        }
    }

    private static class AddDishRecevier extends ResultRecevierCallback<Dish> {
        private WeakReference<AddDishFragment> activityRef;

        public AddDishRecevier(AddDishFragment fragment) {
            activityRef = new WeakReference(fragment);
        }

        @Override
        public void onSuccess(Dish dish) {
            Log.d("TAG", "Success");
            if (activityRef != null && activityRef.get() != null) {
                activityRef.get().onDishAdded(dish);
            }
        }

        @Override
        public void onError(SimpleCookingError error) {

            if (activityRef != null && activityRef.get() != null) {
                activityRef.get().showError(error);
            }
        }
    }


}
