package com.example.treba.simplecooking.data.local_storage.shared_preferences;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Bundle;
import android.os.ResultReceiver;

import com.example.treba.simplecooking.data.simple_cooking_api_client.model.LocalUser;
import com.example.treba.simplecooking.data.simple_cooking_api_client.service.SimplceCookingResultRecevier;

/**
 * Created by treba on 17.02.2018.
 */

public class UserSessionManager {
    private static final String USER_PREFS="UserPreferences";
    private static final String USER_IS_LOGGED_IN="is-logged-in";
    public static final String  USER_GLOBAL_ID="global_id";
    public static final String  USER_NAME="name";
    public static final String  USER_EMAIL="email";
    public static final String  USER_PICTURE="picture";
    public static final String  USER_REFRESH_TOKEN="refresh_token";

    public final Context context;

    public final Editor editor;
    public final SharedPreferences pref;

    public UserSessionManager(Context context) {
        this.context = context;
        pref = context.getSharedPreferences(USER_PREFS,0);
        Editor editor = pref.edit();
        this.editor = editor;
    }

    public LocalUser getCurentUser() {
        boolean isLoggedIn=pref.getBoolean(USER_IS_LOGGED_IN,false);
        LocalUser user;
        if(!isLoggedIn)
            user = new LocalUser(-1,"   Guest","guest@example.com",new Uri.Builder().path("").build(),null);
        else
            user = new LocalUser(pref.getLong(USER_GLOBAL_ID, -1), pref.getString(USER_NAME, "Guest"), pref.getString(USER_EMAIL, "guest@example.com"), new Uri.Builder().path(pref.getString(USER_PICTURE, "")).build(), pref.getString(USER_REFRESH_TOKEN, null));
        return user;
    }


    public void setCurrentUser(LocalUser user){
        editor.putBoolean(USER_IS_LOGGED_IN,true)
                .putLong(USER_GLOBAL_ID, user.getId())
                .putString(USER_NAME,user.getName())
                .putString(USER_EMAIL,user.getEmail())
                .putString(USER_PICTURE,user.getPicture().toString())
                .putString(USER_REFRESH_TOKEN,user.getRefreshToken());
        editor.commit();

    }

    public void deleteCurrentUser(){
        editor.clear();
        editor.commit();
    }

}
