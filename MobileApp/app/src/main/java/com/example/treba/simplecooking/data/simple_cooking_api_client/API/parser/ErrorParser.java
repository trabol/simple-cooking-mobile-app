package com.example.treba.simplecooking.data.simple_cooking_api_client.API.parser;

import com.example.treba.simplecooking.data.simple_cooking_api_client.model.SimpleCookingError;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by treba on 19.01.2018.
 */

public final class ErrorParser {
    public static String ERROR="error";
    public static String CODE="code";
    public static String STATUS="status";
    public static String MESSAGE="message";

    public static SimpleCookingError parse(JSONObject object) {
        if(object==null||!object.has(ERROR)){
            return null;
        }
        try {
            JSONObject jsonError= object.getJSONObject("error");
            return new SimpleCookingError(jsonError.getLong(CODE),jsonError.getLong(STATUS),jsonError.getString(MESSAGE));
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static JSONObject toJSON(SimpleCookingError error){
        JSONObject json=new JSONObject();
        try {
            JSONObject jsonError=new JSONObject();
            jsonError.put(CODE,error.getCode());
            jsonError.put(STATUS,error.getStatus());
            jsonError.put(MESSAGE,error.getMessage());
            json.put(ERROR,jsonError);
            return json;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

    }
}
