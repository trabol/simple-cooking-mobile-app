package com.example.treba.simplecooking;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.treba.simplecooking.data.simple_cooking_api_client.model.Dish;

import java.util.ArrayList;

/**
 * Created by treba on 26.01.2018.
 */

public class DishAdapter extends RecyclerView.Adapter<DishAdapter.DishAdapterViewHolder> {

    private ArrayList<Dish> dishes;
    private ClickHandler mClickHandler;


    public DishAdapter(ClickHandler clickHandler){
        this.mClickHandler=clickHandler;
    }


    public interface ClickHandler{
        public void onClick(Dish dish);
    }

    @Override
    public DishAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.dish_layout,parent,false);
        return new DishAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DishAdapterViewHolder holder, int position) {
        Dish dish = dishes.get(position);
        holder.nameTextView.setText(dish.getName());
        //TODO holder.picture
    }

    @Override
    public int getItemCount() {
        if(dishes!=null&&!dishes.isEmpty()){
            return dishes.size();
        }
        return 0;
    }

    public class DishAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public final TextView nameTextView;
        public final ImageView picture;
        public DishAdapterViewHolder(View itemView) {
            super(itemView);
            nameTextView=(TextView) itemView.findViewById(R.id.dish_name);
            picture=(ImageView) itemView.findViewById(R.id.dish_picture);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int adapterPosition = getAdapterPosition();
            Dish dish = dishes.get(adapterPosition);
            mClickHandler.onClick(dish);
        }
    }

    public void setDishes(ArrayList<Dish> dishes){
        this.dishes=dishes;
        notifyDataSetChanged();
    }
}
