package com.example.treba.simplecooking;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.example.treba.simplecooking.data.simple_cooking_api_client.model.Ingredient;

import java.util.ArrayList;

/**
 * Created by treba on 26.01.2018.
 */

public class IngredientAdapter extends RecyclerView.Adapter<IngredientAdapter.IngredientAdapterViewHolder> {

    private ArrayList<Ingredient> ingredients;
    private ClickHandler mClickHandler;


    public IngredientAdapter(ClickHandler clickHandler){
        ingredients=new ArrayList<Ingredient>();
        this.mClickHandler=clickHandler;
    }


    public interface ClickHandler{
        public void onClick(Ingredient ingredient);
    }

    @Override
    public IngredientAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.ingredient_box,parent,false);
        return new IngredientAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(IngredientAdapterViewHolder holder, int position) {
        Ingredient ingredient = ingredients.get(position);
        holder.nameTextView.setText(ingredient.getName());
        //TODO holder.picture
    }

    @Override
    public int getItemCount() {
        if(ingredients!=null&&!ingredients.isEmpty()){
            return ingredients.size();
        }
        return 0;
    }

    public class IngredientAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public final TextView nameTextView;
        public final ImageView picture;
        public IngredientAdapterViewHolder(View itemView) {
            super(itemView);
            nameTextView=(TextView) itemView.findViewById(R.id.ingredient_name);
            picture=(ImageView) itemView.findViewById(R.id.ingredient_picture);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int adapterPosition = getAdapterPosition();
            Ingredient ingredient=ingredients.get(adapterPosition);
            mClickHandler.onClick(ingredient);
        }
    }

    public void setIngredients(ArrayList<Ingredient> ingredients){
        this.ingredients=ingredients;
        notifyDataSetChanged();
    }

    public ArrayList<Ingredient> getIngredients(){
        return this.ingredients;
    }

    public void addIngredient(Ingredient ingredient){
        this.ingredients.add(ingredient);
        notifyDataSetChanged();
    }
}
