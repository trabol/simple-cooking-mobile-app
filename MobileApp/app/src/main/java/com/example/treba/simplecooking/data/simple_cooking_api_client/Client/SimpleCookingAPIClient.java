package com.example.treba.simplecooking.data.simple_cooking_api_client.Client;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;

import com.example.treba.simplecooking.data.simple_cooking_api_client.model.Dish;
import com.example.treba.simplecooking.data.simple_cooking_api_client.model.Ingredient;
import com.example.treba.simplecooking.data.simple_cooking_api_client.model.LocalUser;
import com.example.treba.simplecooking.data.simple_cooking_api_client.model.User;
import com.example.treba.simplecooking.data.simple_cooking_api_client.service.ResultRecevierCallback;
import com.example.treba.simplecooking.data.simple_cooking_api_client.service.SimplceCookingResultRecevier;
import com.example.treba.simplecooking.data.simple_cooking_api_client.service.SimpleCookingAPIService;

import java.util.ArrayList;

/**
 * Created by treba on 16.01.2018.
 */

public class SimpleCookingAPIClient {//Whatever is under the hood remember not to change basics

    private static SimpleCookingAPIClient mInstance;

    private SimpleCookingAPIClient(){

    }
    public static SimpleCookingAPIClient getInstance(){
        if(mInstance==null){
            mInstance=new SimpleCookingAPIClient();
        }
        return mInstance;
    }

    public  void getUserById(Context context, final long id, final ResultRecevierCallback resultRecevierCallback) {
        Intent intent = new Intent(context, SimpleCookingAPIService.class);
        intent.setAction(SimpleCookingAPIService.ACTION_GET_USER_BY_ID);
        SimplceCookingResultRecevier<User> recevier = new SimplceCookingResultRecevier<User>(new Handler());
        recevier.setCallback(resultRecevierCallback);
        intent.putExtra(SimpleCookingAPIService.PARAM_CALLBACK,recevier);
        long idd=1;
        intent.putExtra(SimpleCookingAPIService.EXTRA_ID,idd);
        context.startService(intent);
    }

    public  void refreshAccessToken(Context context, final long id, final ResultRecevierCallback resultRecevierCallback) {
        Intent intent = new Intent(context, SimpleCookingAPIService.class);
        intent.setAction(SimpleCookingAPIService.ACTION_GET_ACCESS_TOKEN);
        SimplceCookingResultRecevier<LocalUser> recevier = new SimplceCookingResultRecevier<LocalUser>(new Handler());
        recevier.setCallback(resultRecevierCallback);
        intent.putExtra(SimpleCookingAPIService.PARAM_CALLBACK,recevier);
        context.startService(intent);
    }

    public  void authenticateWithGoogle(Context context, String authCode, final ResultRecevierCallback resultRecevierCallback) {
        Intent intent = new Intent(context, SimpleCookingAPIService.class);
        intent.setAction(SimpleCookingAPIService.ACTION_AUTHENTICATE_WITH_GOOGLE);
        SimplceCookingResultRecevier<LocalUser> recevier = new SimplceCookingResultRecevier<LocalUser>(new Handler());
        recevier.setCallback(resultRecevierCallback);
        intent.putExtra(SimpleCookingAPIService.PARAM_CALLBACK,recevier);
        intent.putExtra(SimpleCookingAPIService.EXTRA_AUTH_CODE,authCode);
        context.startService(intent);
    }

    public  void getIngredientById(Context context, long id, final ResultRecevierCallback resultRecevierCallback) {
        Intent intent = new Intent(context, SimpleCookingAPIService.class);
        intent.setAction(SimpleCookingAPIService.ACTION_GET_INGREDIENT_BY_ID);
        SimplceCookingResultRecevier<LocalUser> recevier = new SimplceCookingResultRecevier<LocalUser>(new Handler());
        recevier.setCallback(resultRecevierCallback);
        intent.putExtra(SimpleCookingAPIService.PARAM_CALLBACK,recevier);
        intent.putExtra(SimpleCookingAPIService.EXTRA_ID,id);
        context.startService(intent);
    }

    public  void getIngredientByName(Context context, String name, final ResultRecevierCallback resultRecevierCallback) {
        Log.d("TAG","client "+name);
        Intent intent = new Intent(context, SimpleCookingAPIService.class);
        intent.setAction(SimpleCookingAPIService.ACTION_GET_INGREDIENT_BY_NAME);
        SimplceCookingResultRecevier<Ingredient> recevier = new SimplceCookingResultRecevier<Ingredient>(new Handler());
        recevier.setCallback(resultRecevierCallback);
        intent.putExtra(SimpleCookingAPIService.PARAM_CALLBACK,recevier);
        intent.putExtra(SimpleCookingAPIService.EXTRA_NAME,name);
        context.startService(intent);
    }

    public  void postIngredient(Context context, final Ingredient ingredient, final ResultRecevierCallback resultRecevierCallback) {
        Intent intent = new Intent(context, SimpleCookingAPIService.class);
        intent.setAction(SimpleCookingAPIService.ACTION_POST_INGREDIENT);
        SimplceCookingResultRecevier<User> recevier = new SimplceCookingResultRecevier<User>(new Handler());
        recevier.setCallback(resultRecevierCallback);
        intent.putExtra(SimpleCookingAPIService.PARAM_CALLBACK,recevier);
        intent.putExtra(SimpleCookingAPIService.EXTRA_INGREDIENT,ingredient);
        context.startService(intent);
    }

    public  void getDishById(Context context, long id, final ResultRecevierCallback resultRecevierCallback) {
        Intent intent = new Intent(context, SimpleCookingAPIService.class);
        intent.setAction(SimpleCookingAPIService.ACTION_GET_DISH_BY_ID);
        SimplceCookingResultRecevier<LocalUser> recevier = new SimplceCookingResultRecevier<LocalUser>(new Handler());
        recevier.setCallback(resultRecevierCallback);
        intent.putExtra(SimpleCookingAPIService.PARAM_CALLBACK,recevier);
        intent.putExtra(SimpleCookingAPIService.EXTRA_ID,id);
        context.startService(intent);
    }

    public  void postDish(Context context, Dish dish, final ResultRecevierCallback resultRecevierCallback) {
        Intent intent = new Intent(context, SimpleCookingAPIService.class);
        intent.setAction(SimpleCookingAPIService.ACTION_POST_DISH);
        SimplceCookingResultRecevier<LocalUser> recevier = new SimplceCookingResultRecevier<LocalUser>(new Handler());
        recevier.setCallback(resultRecevierCallback);
        intent.putExtra(SimpleCookingAPIService.PARAM_CALLBACK,recevier);
        intent.putExtra(SimpleCookingAPIService.EXTRA_DISH,dish);
        context.startService(intent);
    }

    public  void updateDish(Context context, Dish dish, final ResultRecevierCallback resultRecevierCallback) {
        Intent intent = new Intent(context, SimpleCookingAPIService.class);
        intent.setAction(SimpleCookingAPIService.ACTION_UPDATE_DISH);
        SimplceCookingResultRecevier<LocalUser> recevier = new SimplceCookingResultRecevier<LocalUser>(new Handler());
        recevier.setCallback(resultRecevierCallback);
        intent.putExtra(SimpleCookingAPIService.PARAM_CALLBACK,recevier);
        intent.putExtra(SimpleCookingAPIService.EXTRA_DISH,dish);
        context.startService(intent);
    }

    public  void deleteDish(Context context, long id, final ResultRecevierCallback resultRecevierCallback) {
        Intent intent = new Intent(context, SimpleCookingAPIService.class);
        intent.setAction(SimpleCookingAPIService.ACTION_DELETE_DISH);
        SimplceCookingResultRecevier<LocalUser> recevier = new SimplceCookingResultRecevier<LocalUser>(new Handler());
        recevier.setCallback(resultRecevierCallback);
        intent.putExtra(SimpleCookingAPIService.PARAM_CALLBACK,recevier);
        intent.putExtra(SimpleCookingAPIService.EXTRA_ID,id);
        context.startService(intent);
    }

    public  void simpleSearch(Context context, ArrayList<Ingredient> ingredients, final ResultRecevierCallback resultRecevierCallback) {
        Intent intent = new Intent(context, SimpleCookingAPIService.class);
        intent.setAction(SimpleCookingAPIService.ACTION_SIMPLE_SEARCH);
        SimplceCookingResultRecevier<LocalUser> recevier = new SimplceCookingResultRecevier<LocalUser>(new Handler());
        recevier.setCallback(resultRecevierCallback);
        intent.putExtra(SimpleCookingAPIService.PARAM_CALLBACK,recevier);
        intent.putExtra(SimpleCookingAPIService.EXTRA_INGREDIENTS,ingredients);
        context.startService(intent);
        Log.d("TAG","search started");
    }




}
