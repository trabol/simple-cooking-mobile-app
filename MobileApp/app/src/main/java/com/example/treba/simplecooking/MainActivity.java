package com.example.treba.simplecooking;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.treba.simplecooking.data.local_storage.SimpleCookingLocalStorageClient;
import com.example.treba.simplecooking.data.local_storage.shared_preferences.UserSessionManager;
import com.example.treba.simplecooking.data.simple_cooking_api_client.API.SimpleCookingAPI;
import com.example.treba.simplecooking.data.simple_cooking_api_client.Client.SimpleCookingAPIClient;
import com.example.treba.simplecooking.data.simple_cooking_api_client.model.Ingredient;
import com.example.treba.simplecooking.data.simple_cooking_api_client.model.LocalUser;
import com.example.treba.simplecooking.data.simple_cooking_api_client.model.SimpleCookingError;
import com.example.treba.simplecooking.data.simple_cooking_api_client.model.User;
import com.example.treba.simplecooking.data.simple_cooking_api_client.service.ResultRecevierCallback;
import com.example.treba.simplecooking.fragment.ShowDishFragment;
import com.example.treba.simplecooking.fragment.base.AddDishFragment;
import com.example.treba.simplecooking.fragment.base.SimpleSearchFragment;

import java.lang.ref.WeakReference;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private NavigationView mNavigationView;
    private DrawerLayout mDrawer;
    private TextView userNameTV;
    private TextView userEmailTV;
    private FloatingActionButton fab;

    private String CURRENT_FRAGMENT_TAG;
    private String SIMPLE_SEARCH_FRAGMENT="simple-search";
    private String ADD_DISH="add-dish";
    private int viewItemIndex;

    private String[] fragmentsTitles;

    private Handler mHandler;

    private int SIGN_IN_REQUEST_CODE=3542;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mHandler=new Handler();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CURRENT_FRAGMENT_TAG=SIMPLE_SEARCH_FRAGMENT;
                viewItemIndex=0;
                loadFragment();
                mDrawer.closeDrawer(GravityCompat.START);
                setToolbarTitle();
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }
        });

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.addDrawerListener(toggle);
        toggle.syncState();
        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(this);
        userNameTV= mNavigationView.getHeaderView(0).findViewById(R.id.user_name_text_view);
        userEmailTV=mNavigationView.getHeaderView(0).findViewById(R.id.user_email_text_view);
        Log.d("TAG",userNameTV.toString());
        fragmentsTitles=getResources().getStringArray(R.array.nav_item_titles);
        //SimpleCookingAPI.localUser=new LocalUser(1,"Kacper Tr","qq",null,"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNTE1OTYzMTc0LCJleHAiOjE1MTU5NjY3NzR9.XXZtVJNGJnB_erCDgZBI7fCgcqAxPZBAWMMcM3Kz9zE",
         //       "1.36be2ac8459d9f627735bf9a3ddeb7cbf252ed4610a3fa359e58f9f55d522d9c6f25ae36e95f4dc7");
        LocalUser user=new UserSessionManager(getApplicationContext()).getCurentUser();
        SimpleCookingAPI.localUser=user;
        updateUserInfo(user);
        viewItemIndex=0;
        CURRENT_FRAGMENT_TAG=SIMPLE_SEARCH_FRAGMENT;
        loadFragment();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_user) {
            if(SimpleCookingAPI.localUser.getId()>0){
                viewItemIndex=3;
            }else{
                Intent intent = new Intent(this,SignInFragment.class);
                startActivityForResult(intent,SIGN_IN_REQUEST_CODE);
                return true;
            }
        } else if (id == R.id.nav_simple_search) {
            CURRENT_FRAGMENT_TAG=SIMPLE_SEARCH_FRAGMENT;
            viewItemIndex=0;

        } else if (id == R.id.nav_add_dish) {
            CURRENT_FRAGMENT_TAG=ADD_DISH;
            viewItemIndex=1;
        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }
        loadFragment();
        mDrawer.closeDrawer(GravityCompat.START);
        setToolbarTitle();
        return true;
    }

    private void loadFragment(){
        final Fragment fragment;
        Log.d("TAG",CURRENT_FRAGMENT_TAG);
        if(getSupportFragmentManager().findFragmentByTag(CURRENT_FRAGMENT_TAG)!=null){
            fragment=getSupportFragmentManager().findFragmentByTag(CURRENT_FRAGMENT_TAG);
        }else{
            fragment=getFragment();
        }
        switch(viewItemIndex){
            case 0:{
                fab.setVisibility(View.GONE);
                break;
            }
            case 3:{
                fab.setVisibility(View.GONE);
                break;
            }
            default:{
                fab.setVisibility(View.VISIBLE);
                break;
            }
        }
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(android.R.anim.fade_in,android.R.anim.fade_out);
                transaction.replace(R.id.frame,fragment,CURRENT_FRAGMENT_TAG);
                transaction.addToBackStack(null);
                transaction.commitAllowingStateLoss();
            }
        };
        if(runnable!=null)runnable.run();

        mDrawer.closeDrawers();

        //invalidateOptionsMenu();
    }

    public void loadFragment(final Fragment fragment){

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(android.R.anim.fade_in,android.R.anim.fade_out);
                transaction.replace(R.id.frame,fragment);
                transaction.addToBackStack(null);
                transaction.commitAllowingStateLoss();
            }
        };
        if(runnable!=null)runnable.run();

        //mDrawer.closeDrawers();

        //invalidateOptionsMenu();
    }

    private Fragment getFragment(){
        switch(viewItemIndex){
            case 0:{

                return new SimpleSearchFragment();
            }
            case 1:return new AddDishFragment();
            default:return new SimpleSearchFragment();
        }

    }
     private void setToolbarTitle(){
        getSupportActionBar().setTitle(fragmentsTitles[viewItemIndex]);
     }
    private void setToolbarTitle(String title){
        getSupportActionBar().setTitle(title);
    }
    private void selectNavMenu() {
        mNavigationView.getMenu().getItem(viewItemIndex).setChecked(true);
    }

    public void GetDishById(View view) {
        long id=2;
        SimpleCookingAPIClient.getInstance().getUserById(this,id,new UserRecevier(this));
    }

    public void showUser(User user){
        Log.d("TAG","success");
        Toast.makeText(MainActivity.this,user.toString(),Toast.LENGTH_LONG).show();
    }

    public void showError(SimpleCookingError error){
        Log.d("TAG","error "+error.getMessage());
        Toast.makeText(MainActivity.this,error.getMessage(),Toast.LENGTH_LONG).show();
    }

    private static class UserRecevier extends ResultRecevierCallback<User> {
        private WeakReference<MainActivity> activityRef;
        public UserRecevier(MainActivity activity){
            activityRef = new WeakReference<MainActivity>(activity);
        }
        @Override
        public void onSuccess(User user) {
            if(activityRef != null && activityRef.get() != null) {
                activityRef.get().showUser(user);

            }
        }

        @Override
        public void onError(SimpleCookingError error) {
            if(activityRef != null && activityRef.get() != null) {
                activityRef.get().showError(error);
            }
        }
    }
    private void showIngredient(Ingredient ingredient){
        Toast.makeText(MainActivity.this,ingredient.toString(),Toast.LENGTH_LONG).show();
    }

    private static class IngredientRecevier extends ResultRecevierCallback<Ingredient>{
        private WeakReference<MainActivity> activityRef;
        public IngredientRecevier(MainActivity activity){
            activityRef = new WeakReference<MainActivity>(activity);
        }
        @Override
        public void onSuccess(Ingredient ingredient) {
            Log.d("TAG","Success");
            if(activityRef!=null&&activityRef.get()!=null){
                activityRef.get().showIngredient(ingredient);
            }
        }

        @Override
        public void onError(SimpleCookingError error) {
            Log.d("TAG","Error");

            if(activityRef!=null&&activityRef.get()!=null){
                activityRef.get().showError(error);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("TAG","activity result "+ requestCode+" "+resultCode);
        if(requestCode==SIGN_IN_REQUEST_CODE){
            Log.d("TAG","sign in request");
            if(resultCode==RESULT_OK){
                Log.d("TAG","result ok");
                LocalUser user = data.getParcelableExtra(SignInFragment.EXTRA_USER);
                Toast.makeText(this,"Succesfully Logged In",Toast.LENGTH_LONG).show();
                updateUserInfo(user);
            }else{
                Toast.makeText(this,"Couldn't Login",Toast.LENGTH_LONG).show();
            }
        }
    }

    private void updateUserInfo(LocalUser user){
        userNameTV.setText(user.getName());
        userEmailTV.setText(user.getEmail());
        SimpleCookingAPI.localUser=user;
    }
}
