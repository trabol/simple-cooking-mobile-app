package com.example.treba.simplecooking.data.simple_cooking_api_client.API.parser;

import com.example.treba.simplecooking.data.simple_cooking_api_client.model.Dish;
import com.example.treba.simplecooking.data.simple_cooking_api_client.model.Ingredient;

import org.json.JSONObject;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.FormBody;
import okhttp3.Request;
import okio.Buffer;

import static org.junit.Assert.*;

/**
 * Created by treba on 22.01.2018.
 */
public class DishParserTest {
    private void compareDishes(Dish d1,Dish d2){
        assertEquals(d1.getId(),d2.getId());
        assertEquals(d1.getName(),d2.getName());
        assertEquals(d1.getRecipe(),d2.getRecipe());
        assertEquals(d1.getAuthorId(),d2.getAuthorId());
        assertEquals(d1.getIngredients().size(),d2.getIngredients().size());
        for(int i=0;i<d1.getIngredients().size();i++){
            assertEquals(d1.getIngredients().get(i).getId(),d2.getIngredients().get(i).getId());
            assertEquals(d1.getIngredients().get(i).getName(),d2.getIngredients().get(i).getName());
        }
    }
    @Test
    public void parse() throws Exception {
        ArrayList<Ingredient> ingredients = new ArrayList<Ingredient>();
        ingredients.add(new Ingredient(14,"egg"));
        ingredients.add(new Ingredient(15,"ketchup"));
        Dish expected = new Dish(58,ingredients,"Egg with Ketchup","boil egg and add ketchup",1);
        JSONObject json = new JSONObject("{\n" +
                "  \"dish\": {\n" +
                "    \"id\": \"58\",\n" +
                "    \"name\": \"Egg with Ketchup\",\n" +
                "    \"recipe\": \"boil egg and add ketchup\",\n" +
                "    \"ingredients\": [\n" +
                "      {\n" +
                "        \"id\": 14,\n" +
                "        \"name\": \"egg\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": 15,\n" +
                "        \"name\": \"ketchup\"\n" +
                "      }\n" +
                "    ],\n" +
                "    \"authorId\": 1\n" +
                "  }\n" +
                "}");
        Dish actual = DishParser.parse(json);
        compareDishes(expected,actual);
    }

    @Test
    public void parseToList() throws Exception {
        ArrayList<Dish> expected= new ArrayList<>();
        ArrayList<Ingredient> ingredients1=new ArrayList<>();
        ingredients1.add(new Ingredient(14,"egg"));
        ingredients1.add(new Ingredient(15,"ketchup"));
        expected.add(new Dish(58,ingredients1,"Egg with Ketchup","boil egg and add ketchup",1));
        ArrayList<Ingredient> ingredients2 = new ArrayList<>();
        ingredients2.add(new Ingredient(14,"egg"));
        ingredients2.add(new Ingredient(16,"pasta"));
        expected.add(new Dish(59,ingredients2,"Egg with Pasta","boil egg and add pasta",1));
        JSONObject json = new JSONObject("{\n" +
                "    \"dishes\": [\n" +
                "        {\n" +
                "            \"id\": 58,\n" +
                "            \"name\": \"Egg with Ketchup\",\n" +
                "            \"recipe\": \"boil egg and add ketchup\",\n" +
                "            \"ingredients\": [\n" +
                "                {\n" +
                "                    \"id\": 14,\n" +
                "                    \"name\": \"egg\"\n" +
                "                },\n" +
                "                {\n" +
                "                    \"id\": 15,\n" +
                "                    \"name\": \"ketchup\"\n" +
                "                }\n" +
                "            ],\n" +
                "            \"authorId\": 1\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 59,\n" +
                "            \"name\": \"Egg with Pasta\",\n" +
                "            \"recipe\": \"boil egg and add pasta\",\n" +
                "            \"ingredients\": [\n" +
                "                {\n" +
                "                    \"id\": 14,\n" +
                "                    \"name\": \"egg\"\n" +
                "                },\n" +
                "                {\n" +
                "                    \"id\": 16,\n" +
                "                    \"name\": \"pasta\"\n" +
                "                }\n" +
                "            ],\n" +
                "            \"authorId\": 1\n" +
                "        }\n" +
                "    ]\n" +
                "}");
        ArrayList<Dish> actual=DishParser.parseToList(json);
        for(int i=0;i<expected.size();i++){
            compareDishes(expected.get(i),actual.get(i));
        }
    }

    private static String bodyToString(final Request request){

        try {
            final Request copy = request.newBuilder().build();
            final Buffer buffer = new Buffer();
            copy.body().writeTo(buffer);
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "did not work";
        }
    }

    @Test
    public void toFormBody() throws Exception {
        FormBody expected = new FormBody.Builder()
                .add("name","Egg with Ketchup")
                .add("recipe","boil egg and add ketchup")
                .add("authorId", "1")
                .add("ingredient","egg")
                .add("ingredient","ketchup").build();
        Request.Builder expectedBuilder=new Request.Builder();
        expectedBuilder.url("http://localhost:8080/api/search/simple").post(expected).build().toString();
        ArrayList<Ingredient> ingredients1=new ArrayList<>();
        ingredients1.add(new Ingredient(14,"egg"));
        ingredients1.add(new Ingredient(15,"ketchup"));
        FormBody actual=DishParser.toFormBody(new Dish(58,ingredients1,"Egg with Ketchup","boil egg and add ketchup",1));
        assertEquals(bodyToString(expectedBuilder.post(expected).build()),bodyToString(new Request.Builder().url("http://localhost:8080/api/search/simple").post(actual).build()));
    }

}