package com.example.treba.simplecooking.data.simple_cooking_api_client.API.parser;

import com.example.treba.simplecooking.data.simple_cooking_api_client.model.SimpleCookingError;

import org.json.JSONObject;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by User on 24.01.2018.
 */
public class ErrorParserTest {
    @Test
    public void parse() throws Exception {
        SimpleCookingError expected=new SimpleCookingError(7,404,"This dish doesn't exist");
        JSONObject json = new JSONObject("{\n" +
                "    \"error\": {\n" +
                "        \"status\": 404,\n" +
                "        \"code\": 7,\n" +
                "        \"message\": \"This dish doesn't exist\"\n" +
                "    }\n" +
                "}");
        SimpleCookingError actual = ErrorParser.parse(json);
        assertEquals(expected.toString(),actual.toString());
    }

    @Test
    public void toJSON() throws Exception {
        JSONObject expected = new JSONObject("{\n" +
                "    \"error\": {\n" +
                "        \"status\": 404,\n" +
                "        \"code\": 7,\n" +
                "        \"message\": \"This dish doesn't exist\"\n" +
                "    }\n" +
                "}");

        SimpleCookingError error=new SimpleCookingError(7,404,"This dish doesn't exist");

        JSONObject actual=ErrorParser.toJSON(error);
        assertEquals(expected.toString(),actual.toString());
    }

}