package com.example.treba.simplecooking.data.simple_cooking_api_client.API.parser;

import com.example.treba.simplecooking.data.simple_cooking_api_client.model.Dish;
import com.example.treba.simplecooking.data.simple_cooking_api_client.model.Ingredient;

import org.json.JSONObject;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by treba on 22.01.2018.
 */
public class IngredientParserTest {
    @Test
    public void parse() throws Exception {
        Ingredient expected =new Ingredient(14,"egg");
        JSONObject json = new JSONObject("{\n" +
                "  \"ingredient\": {\n" +
                "    \"id\": \"14\",\n" +
                "    \"name\": \"egg\"\n" +
                "  }\n" +
                "}");
       Ingredient actual = IngredientParser.parse(json);
       assertEquals(expected.getName(),actual.getName());
       assertEquals(expected.getId(),actual.getId());
    }

    @Test
    public void parseToList() throws Exception {
        ArrayList<Ingredient> expecteds = new ArrayList<>();
        expecteds.add(new Ingredient(14,"egg"));
        expecteds.add(new Ingredient(15,"ketchup"));
        JSONObject json = new JSONObject("{\n" +
                "\t\"ingredients\": [{\n" +
                "\t\t\t\"id\": 14,\n" +
                "\t\t\t\"name\": \"egg\"\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"id\": 15,\n" +
                "\t\t\t\"name\": \"ketchup\"\n" +
                "\t\t}\n" +
                "\t]\n" +
                "}");
        ArrayList<Ingredient> actuals = IngredientParser.parseToList(json);
        assertEquals(expecteds.size(),actuals.size());
        for(int i=0;i<expecteds.size();i++) {
            Ingredient expected = expecteds.get(i);
            Ingredient actual = actuals.get(i);
            assertEquals(expected.getName(), actual.getName());
            assertEquals(expected.getId(), actual.getId());
        }
    }

}