package com.example.treba.simplecooking.data.simple_cooking_api_client.API.parser;

import android.net.Uri;

import com.example.treba.simplecooking.data.simple_cooking_api_client.model.LocalUser;
import com.example.treba.simplecooking.data.simple_cooking_api_client.model.User;

import org.json.JSONObject;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by User on 24.01.2018.
 */
public class UserParserTest {
    @Test
    public void parse() throws Exception {
        User expected= new User(1,"Kacper Tr","trebacz.kacper@gmail.com",new Uri.Builder().path("https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50").build());
        JSONObject json = new JSONObject("{\"user\":{\"id\":1,\"name\":\"Kacper Tr\",\"email\":\"trebacz.kacper@gmail.com\",\"picture\":\"https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50\",\"refreshToken\":\"1.36be2ac8459d9f627735bf9a3ddeb7cbf252ed4610a3fa359e58f9f55d522d9c6f25ae36e95f4dc7\",\"accessToken\":\"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNTE1OTYzMTc0LCJleHAiOjE1MTU5NjY3NzR9.XXZtVJNGJnB_erCDgZBI7fCgcqAxPZBAWMMcM3Kz9zE\"}}");
        User actual = UserParser.parse(json);
        assertEquals(expected.toString(),actual.toString());
    }

}